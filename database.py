import os
import random
import string
import functools
import psycopg2
from psycopg2 import sql
from datetime import datetime
from dotenv import load_dotenv


class PacomeDB(object):
    def __init__(self, *args):
        super().__init__()
        load_dotenv()
        self.ID_SIZE = int(os.getenv("ID_SIZE"))
        if len(args):
            assert isinstance(args[0], str)
            self.DB_NAME = args[0]
        else:
            self.DB_NAME = os.getenv("DB_NAME")

    def connected_request(func):
        @functools.wraps(func)
        def wrapper(self, *args, **kwargs):
            # conn = sqlite3.connect(f"{self.DB_NAME}.db")
            conn = psycopg2.connect(database=self.DB_NAME,
                                    user=os.getenv("DB_USERNAME"),
                                    password=os.getenv("DB_PASSWORD"),
                                    host=os.getenv("DB_HOST"),
                                    port=os.getenv("DB_PORT"))
            conn.autocommit = True
            c = conn.cursor()
            res = func(self, c, *args, **kwargs)
            conn.close()
            return res
        return wrapper

    # Fancy
    @connected_request
    def _getGroupCode(self, c):
        group_code = ''.join(random.SystemRandom().choice(string.ascii_uppercase
                                                          + string.ascii_lowercase
                                                          + string.digits) for _ in range(self.ID_SIZE))
        c.execute(f"SELECT COUNT(*) FROM groups WHERE group_code='{group_code}'")
        count = c.fetchone()[0]
        if count:
            group_code = self.getGoupId()
        return group_code

    # Fancy
    @connected_request
    def getAllUsers(self, c):
        c.execute('SELECT * FROM users')
        users = c.fetchall()
        return users

    # Generic
    @connected_request
    def getUser(self, c, *fields, **conditions):
        sql = 'SELECT {} FROM users'
        if fields:
            sql = sql.format(', '.join(fields))
        else:
            sql.format('*')
        if conditions:
            clause = ' WHERE {}'.format(' AND '.join('{}=%s'.format(k) for k in conditions))
            sql = sql + clause
        c.execute(sql, tuple(conditions.values()))
        result = c.fetchall()
        return result

    # Generic
    @connected_request
    def getGroup(self, c, *fields, **conditions):
        sql = 'SELECT {} FROM groups'
        if fields:
            sql = sql.format(', '.join(fields))
        else:
            sql.format('*')
        if conditions:
            clause = ' WHERE {}'.format(' AND '.join('{}=%s'.format(k) for k in conditions))
            sql = sql + clause
        c.execute(sql, tuple(conditions.values()))
        result = c.fetchall()
        return result

    # Generic
    @connected_request
    def getUserGroupRole(self, c, *fields, **conditions):
        sql = 'SELECT {} FROM user_group_role'
        if fields:
            sql = sql.format(', '.join(fields))
        else:
            sql.format('*')
        if conditions:
            clause = ' WHERE {}'.format(' AND '.join('{}=%s'.format(k) for k in conditions))
            sql = sql + clause
        c.execute(sql, tuple(conditions.values()))
        result = c.fetchall()
        return result

    # Generic
    @connected_request
    def getRole(self, c, *fields, **conditions):
        sql = 'SELECT {} FROM roles'
        if fields:
            sql = sql.format(', '.join(fields))
        else:
            sql = sql.format('*')
        if conditions:
            clause = ' WHERE {}'.format(' AND '.join('{}=%s'.format(k) for k in conditions))
            sql = sql + clause
        c.execute(sql, tuple(conditions.values()))
        result = c.fetchall()
        return result

    # Generic
    @connected_request
    def getTrack(self, c, *fields, **conditions):
        sql = 'SELECT {} FROM tracks'
        if fields:
            sql = sql.format(', '.join(fields))
        else:
            sql = sql.format('*')
        if conditions:
            clause = ' WHERE {}'.format(' AND '.join('{}=%s'.format(k) for k in conditions))
            sql = sql + clause
        c.execute(sql, tuple(conditions.values()))
        result = c.fetchall()
        return result

    # Generic
    @connected_request
    def updateTrack(self, c, track_id, **kwargs):
        sql = 'UPDATE tracks SET {} WHERE id={}'.format(', '.join('{}=%s'.format(k) for k in kwargs), track_id)
        c.execute(sql, tuple(kwargs.values()))

    # Fancy
    @connected_request
    def createSpAuth(self, c, token_data):
        fields = ['user_id', 'sp_user_id', 'token', 'token_type', 'refresh_token', 'expire_time', 'token_birth']
        sql = f"INSERT INTO sp_auth ({', '.join(fields)}) VALUES (%s, %s, %s, %s, %s, %s, %s) RETURNING id"
        t = tuple([token_data[f] for f in fields])
        c.execute(sql, t)
        sp_auth_id = c.fetchone()[0]
        return sp_auth_id

    # Fancy
    @connected_request
    def getSpAuth(self, c, group_id):
        fields = ['user_id', 'sp_user_id', 'token', 'token_type', 'refresh_token', 'expire_time', 'token_birth']
        sql = f"SELECT {', '.join(fields)} FROM sp_auth WHERE user_id=(SELECT user_id FROM user_group_role WHERE group_id=%s AND role_id=(SELECT id FROM roles WHERE name='owner'))"
        t = (group_id,)
        c.execute(sql, t)
        value = c.fetchone()
        if value:
            return dict(zip(fields, value))
        return None

    # Fancy
    @connected_request
    def delSpAuth(self, c, user_id):
        sql = "DELETE FROM sp_auth WHERE user_id=%s"
        t = (user_id,)
        c.execute(sql, t)

    # Fancy
    @connected_request
    def isSpLogged(self, c, user_id):
        c.execute("SELECT COUNT(*) FROM sp_auth WHERE user_id=%s", (user_id,))
        count = c.fetchone()[0]
        return bool(count)

    # Fancy
    @connected_request
    def updateSpAuth(self, c, token_data):
        fields = ['user_id', 'sp_user_id', 'token', 'token_type', 'refresh_token', 'expire_time', 'token_birth']
        id = token_data.pop('id', None)
        req = sql.SQL("UPDATE {table} SET {fields} WHERE id=%s").format(
                table=sql.Identifier('sp_auth'),
                fields=sql.SQL(', ').join([
                    sql.SQL('=').join([sql.Identifier(field), sql.Placeholder()])
                    for field in fields if field != 'id'
                    ]),
                )
        values = [token_data[f] for f in fields]
        values.append(id)
        t = tuple(values)
        c.execute(req, t)

    # Fancy
    @connected_request
    def createTrack(self, c, user_id, group_id, track_url, action):
        sql = "INSERT INTO tracks (group_id, user_id, track_url, action, date) VALUES (%s, %s, %s, %s, %s) RETURNING id"
        t = (group_id, user_id, track_url, action, datetime.utcnow().isoformat())
        c.execute(sql, t)
        track_id = c.fetchone()[0]
        return track_id

    @connected_request
    def get_pending_tracks(self, c, group):
        sql = "SELECT * FROM tracks WHERE group_id=%s AND dead=false"
        c.execute(sql, (group.id,))
        return c.fetchall()

    # Fancy
    @connected_request
    def createVote(self, c, user_id, track_id, vote):
        sql = "INSERT INTO votes (user_id, track_id, vote, date) VALUES (%s, %s, %s, %s) RETURNING id"
        t = (user_id, track_id, vote, datetime.utcnow().isoformat())
        c.execute(sql, t)
        vote_id = c.fetchone()[0]
        return vote_id

    # Fancy
    @connected_request
    def hasVoted(self, c, user_id, track_id):
        c.execute('SELECT COUNT(*) FROM votes WHERE user_id=%s AND track_id=%s', (user_id, track_id))
        count = c.fetchone()[0]
        return bool(count)

    # Fancy
    @connected_request
    def isMember(self, c, user_id, group_id):
        c.execute('SELECT COUNT(*) FROM user_group_role WHERE user_id=%s AND group_id=%s', (user_id, group_id))
        count = c.fetchone()[0]
        return bool(count)

    # Fancy
    @connected_request
    def getUserGroupRoles(self, c, user_id):
        sql = 'SELECT g.id, g.group_name, g.group_code, r.name FROM groups g,\
                roles r WHERE (g.id, r.id)\
                IN\
                (SELECT group_id, role_id FROM user_group_role WHERE user_id=%s);'
        c.execute(sql, (user_id,))
        result = c.fetchall()
        return result

    # Fancy
    @connected_request
    def getUserId(self, c, user_tgid):
        c.execute("SELECT id FROM users WHERE user_tgid=%s", (str(user_tgid),))
        users = c.fetchone()
        if users:
            user_id = users[0]
            return user_id
        else:
            raise PacomeDB.UnknownUserError("Unknown user", "Check if it is present in the database")

    # Fancy
    @connected_request
    def getUserLang(self, c, user_tgid):
        c.execute("SELECT user_lang FROM users WHERE user_tgid=%s", (str(user_tgid),))
        users = c.fetchone()
        if users:
            user_lang = users[0]
            return user_lang
        else:
            raise PacomeDB.UnknownUserError("Unknown user", "Check if it is present in the database")

    # Generic
    @connected_request
    def updateUser(self, c, user_id, **kwargs):
        sql = 'UPDATE users SET {} WHERE id={}'.format(', '.join('{}=%s'.format(k) for k in kwargs), user_id)
        c.execute(sql, tuple(kwargs.values()))

    # Generic
    @connected_request
    def updateGroup(self, c, group_id, **kwargs):
        sql = 'UPDATE groups SET {} WHERE id={}'.format(', '.join('{}=%s'.format(k) for k in kwargs), group_id)
        c.execute(sql, tuple(kwargs.values()))

    # Fancy
    @connected_request
    def getUserActivePlaylist(self, c, user_tgid):
        c.execute('SELECT group_playlist_id FROM groups WHERE id=(SELECT user_active_group FROM users WHERE user_tgid=%s)', (str(user_tgid),))
        playlist = c.fetchone()
        return playlist[0]

    # Generic
    @connected_request
    def createGroup(self, c, name, user_id, playlist_url, playlist_id, group_threshold):
        group_code = self._getGroupCode()

        c.execute("SELECT id FROM roles WHERE name='owner'")
        owner_role = c.fetchone()[0]

        c.execute('INSERT INTO groups (group_code, group_name, group_playlist_url, group_playlist_id, group_threshold) VALUES (%s, %s, %s, %s, %s) RETURNING id', (group_code, name, playlist_url, playlist_id, group_threshold))
        group_id = c.fetchone()[0]
        c.execute('INSERT INTO user_group_role (user_id, group_id, role_id) VALUES (%s, %s, %s)', (user_id, group_id, owner_role))
        c.execute('UPDATE users SET user_active_group=%s WHERE id=%s', (group_id, user_id))
        return group_id, name, group_code, playlist_url

    # Fancy
    @connected_request
    def deleteGroup(self, c, group_id):
        c.execute('UPDATE users SET user_active_group=NULL WHERE user_active_group=%s', (group_id,))
        c.execute("DELETE FROM user_group_role WHERE group_id=%s", (group_id,))
        c.execute("DELETE FROM votes WHERE track_id IN (SELECT id FROM tracks WHERE group_id=%s)", (group_id,))
        c.execute("DELETE FROM tracks WHERE group_id=%s", (group_id,))
        c.execute("DELETE FROM groups WHERE id=%s", (group_id,))

    # Generic
    @connected_request
    def registerUser(self, c, user_tgid):
        c.execute("SELECT id FROM users WHERE user_tgid=%s", (str(user_tgid), ))
        users = c.fetchone()
        if users is None:
            c.execute("INSERT INTO users (user_tgid) VALUES (%s) RETURNING id", (str(user_tgid),))
            user_id = c.fetchone()[0]
        else:
            user_id = users[0]
        return user_id

    # Fancy
    @connected_request
    def createBinding(self, c, chan_tgid, group_id):
        c.execute('INSERT INTO chan_group (group_id, chan_tgid) VALUES (%s, %s)', (group_id, str(chan_tgid)))

    # Fancy
    @connected_request
    def updateBinding(self, c, chan_tgid, group_id):
        c.execute('UPDATE chan_group SET group_id=%s WHERE chan_tgid=%s', (group_id, str(chan_tgid)))

    # Fancy
    @connected_request
    def getChanBinding(self, c, chan_tgid):
        c.execute('SELECT group_id FROM chan_group WHERE chan_tgid=%s', (str(chan_tgid),))
        group_id = c.fetchone()
        if group_id:
            return group_id[0]
        else:
            return None

    # Fancy
    @connected_request
    def join(self, c, user_id, group_code):
        c.execute('SELECT id, group_name, group_playlist_url  FROM groups WHERE group_code=%s', (group_code,))
        group = c.fetchone()
        if group:
            if not self.isMember(user_id, group[0]):
                c.execute("SELECT id FROM roles WHERE name='regular'")
                regular_role = c.fetchone()[0]
                c.execute('INSERT INTO user_group_role (group_id, user_id, role_id) VALUES (%s, %s, %s)', (group[0], user_id, regular_role))
                c.execute('UPDATE users SET user_active_group=%s WHERE id=%s', (group[0], user_id))
            return group[1:]
        else:
            raise self.UnknownGroupError("Unknown Group", "Make sure is has been created")

    # Fancy
    @connected_request
    def leave(self, c, user_id, group_code):
        c.execute('SELECT id, group_name FROM groups WHERE group_code=%s', (group_code,))
        group = c.fetchone()
        if group:
            c.execute('DELETE FROM user_group_role WHERE group_id=%s AND user_id=%s', (group[0], user_id))
            c.execute('UPDATE users SET user_active_group=NULL WHERE id=%s', (user_id,))
            return group[1]
        else:
            raise self.UnknownGroupError("Unknown Group", "Make sure is has been created")

    @connected_request
    def delete(self, c):
        c.execute("TRUNCATE groups CASCADE")
        c.execute("TRUNCATE users CASCADE")
        # c.execute("TRUNCATE roles CASCADE")
        c.execute("TRUNCATE user_group_role CASCADE")
        c.execute("TRUNCATE user_group CASCADE")
        c.execute("TRUNCATE chan_group CASCADE")
        c.execute("TRUNCATE tracks CASCADE")
        c.execute("TRUNCATE votes CASCADE")
        # os.remove(f"{self.DB_NAME}.db")

    class UnknownUserError(Exception):
        """Exception raised for inexistant playlists.

        Attributes:
            expression -- input expression in which the error occurred
            message -- explanation of the error
        """

        def __init__(self, expression, message):
            self.expression = expression
            self.message = message

    class UnknownGroupError(Exception):
        """Exception raised for inexistant playlists.

        Attributes:
            expression -- input expression in which the error occurred
            message -- explanation of the error
        """

        def __init__(self, expression, message):
            self.expression = expression
            self.message = message
