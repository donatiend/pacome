import os
import sys
import importlib.util
import traceback
import optparse
import psycopg2
from dotenv import load_dotenv
from datetime import datetime

revert = None
over = None


def backup():
    load_dotenv()
    filename = f'backup_{datetime.now().strftime("%Y-%m-%dT%H:%M:%S")}.sql'
    try:
        con = psycopg2.connect(database=os.getenv("DB_NAME"), user=os.getenv("DB_USERNAME"),
                               password=os.getenv("DB_PASSWORD"), host=os.getenv("DB_HOST"),
                               port=os.getenv("DB_PORT"))
        cur = con.cursor()
        cur.execute("""SELECT table_name FROM information_schema.tables
                        WHERE table_schema = 'public'""")

        for table in cur.fetchall():
            table = table[0]
            cur.execute(f'SELECT * FROM {table}')
            f = open(filename, 'a')
            for row in cur.fetchall():
                f.write(f"insert into {table} values ({str(row)});\n")

    except psycopg2.DatabaseError as e:
        print('Error %s', e)
        sys.exit(1)

    finally:
        if con:
            con.close()


def migrate(revert=None, mfrom=None, mto=None):
    if revert:
        for file in sorted(os.listdir("migrations/"), reverse=True):
            if file.endswith('.py'):
                if (not mfrom or int(file[0:4]) >= mfrom) and (not mto or int(file[0:4]) <= mto):
                    package_name = file.replace('.py', '')
                    spec = importlib.util.spec_from_file_location(package_name, f"migrations/{file}")
                    migration = importlib.util.module_from_spec(spec)
                    spec.loader.exec_module(migration)
                    migration.__init__()
                    migration.__revert__()
    else:
        for file in sorted(os.listdir("migrations/")):
            if file.endswith('.py'):
                if (not mfrom or int(file[0:4]) >= mfrom) and (not mto or int(file[0:4]) <= mto):
                    package_name = file.replace('.py', '')
                    spec = importlib.util.spec_from_file_location(package_name, f"migrations/{file}")
                    migration = importlib.util.module_from_spec(spec)
                    spec.loader.exec_module(migration)
                    migration.__init__()
                    try:
                        migration.__migrate__()
                    except Exception as e:
                        print("There was an issue")
                        traceback.print_exc()
                        print(e)
                        migration.__revert__()
                        sys.exit()


if __name__ == "__main__":
    parser = optparse.OptionParser()

    parser.add_option('-f', '--from',
                      action="store", dest="mfrom", type='int',
                      help="min migration num", default=0)
    parser.add_option('-t', '--to',
                      action="store", dest="mto", type='int',
                      help="max migration num", default=0)
    parser.add_option('-r', '--revert',
                      action="store_true", dest="revert",
                      help="Revert migration", default=False)
    parser.add_option('-b', '--backup',
                      action="store_true", dest="backup",
                      help="Revert migration", default=False)

    options, args = parser.parse_args()

    if options.backup:
        backup()
    migrate(options.revert, options.mfrom, options.mto)
