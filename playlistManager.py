from spotify import SpotifyAPI
from datetime import datetime, timedelta
from urllib.parse import urlparse
from tzlocal import get_localzone
import pytz


class PlaylistManager(object):
    def __init__(self, db):
        super().__init__()
        self.db = db
        self.spApi = SpotifyAPI(db)

    def clean_url(self, url_string):
        o = urlparse(url_string)
        return o.scheme + "://" + o.netloc + o.path

    def login(self, user_id):
        if self.db.isSpLogged(user_id):
            return
        return self.spApi.login(user_id)

    def createPlaylist(self, group_id, name):
        _, (url, spotify_id, snapshot) = self.spApi.createPlaylist(group_id, name)
        return url, spotify_id, snapshot

    def get_pending(self, group):
        return self.db.get_pending_tracks(group)

    def deletePlaylist(self, group_id, playlist_id):
        self.spApi.deletePlaylist(group_id, playlist_id)

    def uploadCover(self, image, group_id):
        playlist_id = self.db.getGroup("group_playlist_id", id=group_id)[0][0]
        self.spApi.uploadCoverPicture(group_id, playlist_id, image)

    def uploadDesc(self, desc, group_id):
        playlist_id = self.db.getGroup("group_playlist_id", id=group_id)[0][0]
        self.spApi.uploadDescription(group_id, playlist_id, desc)

    def addTrack(self, track_url, group_id, user_id):
        track_url = self.clean_url(track_url)
        track_uri = self.spApi.parseTrack(group_id, track_url)
        track_id = self.db.createTrack(user_id, group_id, track_uri, 1)
        return track_url, track_id, group_id

    def addTracks(self, tracks, group_id, user_id):
        assert type(tracks) == list
        results = [self.addTrack(track, group_id, user_id) for track in tracks]
        tracks_url = [res[0] for res in results]
        tracks_id = [res[1] for res in results]
        return tracks_url, tracks_id, group_id

    def removeTrack(self, track_url, group_id, user_id):
        track_url = self.clean_url(track_url)
        track_uri = self.spApi.parseTrack(group_id, track_url)
        track_id = self.db.createTrack(user_id, group_id, track_uri, 0)
        return track_url, track_id, group_id

    def removeTracks(self, tracks, group_id, user_id):
        assert type(tracks) == list
        results = [self.removeTrack(track, group_id, user_id) for track in tracks]
        tracks_url = [res[0] for res in results]
        tracks_id = [res[1] for res in results]
        return tracks_url, tracks_id, group_id

    def vote(self, user_id, track_id, vote):
        if vote == 1:
            vote_value = self.db.getTrack("votes_for", id=track_id)[0][0] + 1
            self.db.updateTrack(track_id, votes_for=vote_value)
        elif vote == 0:
            vote_value = self.db.getTrack("votes_against", id=track_id)[0][0] + 1
            self.db.updateTrack(track_id, votes_against=vote_value)
        else:
            print("Raise some exception or something")
        self.db.createVote(user_id, track_id, vote)
        self.trackUpdate()

    def trackUpdate(self):
        all_tracks = self.db.getTrack()

        for track in all_tracks:
            # Maybe have timedelta as a parameter
            local_tz = get_localzone()
            tz_local_now = datetime.now().replace(tzinfo=pytz.utc).astimezone(local_tz)
            if track[8]:
                continue
            elif track[7] < tz_local_now - timedelta(days=7):
                self.db.updateTrack(track[0], dead=True)
            else:
                group_threshold, playlist_id = self.db.getGroup("group_threshold", "group_playlist_id", id=track[1])[0]
                if track[4] - track[5] >= group_threshold:
                    if track[6]:
                        self._addTrackSpotify(track[1], track[3], playlist_id)
                    else:
                        self._removeTrackSpotify(track[1], track[3], playlist_id)
                    self.db.updateTrack(track[0], dead=True)

    def _addTrackSpotify(self, group_id, track, playlist_id):
        _ = self.spApi.addTracks(group_id, playlist_id, [track])

    def _addTracksSpotify(self, group_id, tracks, playlist_id):
        assert type(tracks) == list
        _ = self.spApi.addTracks(group_id, playlist_id, tracks)

    def _removeTrackSpotify(self, group_id, track, playlist_id):
        _ = self.spApi.removeTracks(group_id, playlist_id, [track])

    def _removeTracksSpotify(self, group_id, tracks, playlist_id):
        assert type(tracks) == list
        _ = self.spApi.removeTracks(group_id, playlist_id, tracks)
