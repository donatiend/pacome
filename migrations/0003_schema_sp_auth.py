import os
from dotenv import load_dotenv

import psycopg2
from psycopg2 import sql
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT


def create_sp_auth():

    con = psycopg2.connect(database=os.getenv("DB_NAME"), user=os.getenv("DB_USERNAME"),
                           password=os.getenv("DB_PASSWORD"), host=os.getenv("DB_HOST"),
                           port=os.getenv("DB_PORT"))

    con.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
    cur = con.cursor()

    cur.execute("CREATE TABLE IF NOT EXISTS sp_auth (\
            id SERIAL PRIMARY KEY,\
            user_id INTEGER REFERENCES users(id),\
            sp_user_id TEXT,\
            token TEXT,\
            token_type TEXT,\
            refresh_token TEXT,\
            expire_time INTEGER,\
            token_birth TIMESTAMPTZ)")
    con.commit()
    con.close()


def revert_create_sp_auth():
    con = psycopg2.connect(database=os.getenv("DB_NAME"), user=os.getenv("DB_USERNAME"),
                           password=os.getenv("DB_PASSWORD"), host=os.getenv("DB_HOST"),
                           port=os.getenv("DB_PORT"))

    con.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)

    cur = con.cursor()

    cur.execute(sql.SQL("DROP TABLE {}").format(
        sql.Identifier("sp_auth"))
    )
    con.commit()
    con.close()


def __init__():
    load_dotenv()


def __migrate__():
    create_sp_auth()


def __revert__():
    revert_create_sp_auth()


if __name__ == "__main__":
    __init__()
    try:
        __migrate__()
    finally:
        # print("No revert")
        __revert__()
