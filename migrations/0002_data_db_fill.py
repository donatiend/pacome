import os

from dotenv import load_dotenv

import sqlite3
import psycopg2


def migrate_users():
    new_db = psycopg2.connect(database=os.getenv("DB_NAME"), user=os.getenv("DB_USERNAME"),
                              password=os.getenv("DB_PASSWORD"), host=os.getenv("DB_HOST"),
                              port=os.getenv("DB_PORT"))

    old_db = sqlite3.connect(f"{os.getenv('DB_NAME')}.db")

    old_cur = old_db.cursor()
    new_cur = new_db.cursor()

    old_cur.execute("SELECT * FROM users")
    users = old_cur.fetchall()

    for user in users:
        new_cur.execute("INSERT INTO users (id, user_tgid, user_active_group, user_lang) VALUES (%s, %s, %s, %s)", user)
    new_cur.execute("SELECT setval('users_id_seq', (SELECT MAX(id) FROM users)+1)")
    new_db.commit()
    new_db.close()
    old_db.close()


def migrate_groups():
    new_db = psycopg2.connect(database=os.getenv("DB_NAME"), user=os.getenv("DB_USERNAME"),
                              password=os.getenv("DB_PASSWORD"), host=os.getenv("DB_HOST"),
                              port=os.getenv("DB_PORT"))

    old_db = sqlite3.connect(f"{os.getenv('DB_NAME')}.db")

    old_cur = old_db.cursor()
    new_cur = new_db.cursor()

    old_cur.execute("SELECT * FROM groups")
    groups = old_cur.fetchall()

    for group in groups:
        new_cur.execute("INSERT INTO groups (id, group_code, group_name, group_playlist_url, group_playlist_id, group_threshold) VALUES (%s, %s, %s, %s, %s, %s)",
                        (group[0], group[1], group[2], group[3], group[4], group[6]))
    new_cur.execute("SELECT setval('groups_id_seq', (SELECT MAX(id) FROM groups)+1)")
    new_db.commit()
    new_db.close()
    old_db.close()


def migrate_roles():
    con = psycopg2.connect(database=os.getenv("DB_NAME"), user=os.getenv("DB_USERNAME"),
                           password=os.getenv("DB_PASSWORD"), host=os.getenv("DB_HOST"),
                           port=os.getenv("DB_PORT"))
    cur = con.cursor()
    cur.execute("INSERT INTO roles (name, description) VALUES ('admin', 'Users with this role have full privileges')")
    cur.execute("SELECT setval('roles_id_seq', (SELECT MAX(id) FROM roles)+1)")
    con.commit()
    con.close()


def migrate_user_group_role():
    new_db = psycopg2.connect(database=os.getenv("DB_NAME"), user=os.getenv("DB_USERNAME"),
                              password=os.getenv("DB_PASSWORD"), host=os.getenv("DB_HOST"),
                              port=os.getenv("DB_PORT"))

    old_db = sqlite3.connect(f"{os.getenv('DB_NAME')}.db")

    old_cur = old_db.cursor()
    new_cur = new_db.cursor()

    old_cur.execute("SELECT id, group_admin FROM groups")
    groups = old_cur.fetchall()
    new_cur.execute("SELECT id FROM roles WHERE name='admin'")
    admin_role = new_cur.fetchone()[0]

    for group in groups:
        new_cur.execute("INSERT INTO user_group_role (user_id, group_id, role_id) VALUES (%s, %s, %s)", (group[1], group[0], admin_role))
    new_cur.execute("SELECT setval('user_group_role_id_seq', (SELECT MAX(id) FROM user_group_role)+1)")

    new_db.commit()
    new_db.close()


def migrate_user_group():
    new_db = psycopg2.connect(database=os.getenv("DB_NAME"), user=os.getenv("DB_USERNAME"),
                              password=os.getenv("DB_PASSWORD"), host=os.getenv("DB_HOST"),
                              port=os.getenv("DB_PORT"))

    old_db = sqlite3.connect(f"{os.getenv('DB_NAME')}.db")

    old_cur = old_db.cursor()
    new_cur = new_db.cursor()

    old_cur.execute("SELECT * FROM userGroupRel")
    rels = old_cur.fetchall()

    for rel in rels:
        new_cur.execute("INSERT INTO user_group (id, group_id, user_id) VALUES (%s, %s, %s)", rel)
    new_cur.execute("SELECT setval('user_group_id_seq', (SELECT MAX(id) FROM user_group)+1)")

    new_db.commit()
    new_db.close()


def migrate_chan_group():
    new_db = psycopg2.connect(database=os.getenv("DB_NAME"), user=os.getenv("DB_USERNAME"),
                              password=os.getenv("DB_PASSWORD"), host=os.getenv("DB_HOST"),
                              port=os.getenv("DB_PORT"))

    old_db = sqlite3.connect(f"{os.getenv('DB_NAME')}.db")

    old_cur = old_db.cursor()
    new_cur = new_db.cursor()

    old_cur.execute("SELECT * FROM chanGroupBinding")
    binds = old_cur.fetchall()

    for bind in binds:
        new_cur.execute("INSERT INTO chan_group (id, group_id, chan_tgid) VALUES (%s, %s, %s)", bind)
    new_cur.execute("SELECT setval('chan_group_id_seq', (SELECT MAX(id) FROM chan_group)+1)")

    new_db.commit()
    new_db.close()


def migrate_tracks():
    new_db = psycopg2.connect(database=os.getenv("DB_NAME"), user=os.getenv("DB_USERNAME"),
                              password=os.getenv("DB_PASSWORD"), host=os.getenv("DB_HOST"),
                              port=os.getenv("DB_PORT"))

    old_db = sqlite3.connect(f"{os.getenv('DB_NAME')}.db")

    old_cur = old_db.cursor()
    new_cur = new_db.cursor()

    old_cur.execute("SELECT * FROM trackStack")
    tracks = old_cur.fetchall()

    for track in tracks:
        new_cur.execute("INSERT INTO tracks (id, group_id, user_id, track_url, votes_for, votes_against, action, date, dead)\
                VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)", (track[0], track[1], track[2], track[3], track[4], track[5], track[6], track[7], bool(track[8])))
    new_cur.execute("SELECT setval('tracks_id_seq', (SELECT MAX(id) FROM tracks)+1)")

    new_db.commit()
    new_db.close()


def migrate_votes():
    new_db = psycopg2.connect(database=os.getenv("DB_NAME"), user=os.getenv("DB_USERNAME"),
                              password=os.getenv("DB_PASSWORD"), host=os.getenv("DB_HOST"),
                              port=os.getenv("DB_PORT"))

    old_db = sqlite3.connect(f"{os.getenv('DB_NAME')}.db")

    old_cur = old_db.cursor()
    new_cur = new_db.cursor()

    old_cur.execute("SELECT * FROM votes")
    votes = old_cur.fetchall()

    for vote in votes:
        new_cur.execute("INSERT INTO votes (id, user_id, track_id, vote, date)\
                VALUES (%s, %s, %s, %s, %s)", vote)
    new_cur.execute("SELECT setval('votes_id_seq', (SELECT MAX(id) FROM votes)+1)")
    new_db.commit()
    new_db.close()

# Revert


def revert_migrate_users():
    con = psycopg2.connect(database=os.getenv("DB_NAME"), user=os.getenv("DB_USERNAME"),
                           password=os.getenv("DB_PASSWORD"), host=os.getenv("DB_HOST"),
                           port=os.getenv("DB_PORT"))

    cur = con.cursor()
    cur.execute("TRUNCATE users CASCADE")
    con.commit()
    con.close()


def revert_migrate_groups():
    con = psycopg2.connect(database=os.getenv("DB_NAME"), user=os.getenv("DB_USERNAME"),
                           password=os.getenv("DB_PASSWORD"), host=os.getenv("DB_HOST"),
                           port=os.getenv("DB_PORT"))

    cur = con.cursor()
    cur.execute("TRUNCATE groups CASCADE")
    con.commit()
    con.close()


def revert_migrate_roles():
    con = psycopg2.connect(database=os.getenv("DB_NAME"), user=os.getenv("DB_USERNAME"),
                           password=os.getenv("DB_PASSWORD"), host=os.getenv("DB_HOST"),
                           port=os.getenv("DB_PORT"))

    cur = con.cursor()
    cur.execute("TRUNCATE roles CASCADE")
    con.commit()
    con.close()


def revert_migrate_user_group_role():
    con = psycopg2.connect(database=os.getenv("DB_NAME"), user=os.getenv("DB_USERNAME"),
                           password=os.getenv("DB_PASSWORD"), host=os.getenv("DB_HOST"),
                           port=os.getenv("DB_PORT"))

    cur = con.cursor()
    cur.execute("TRUNCATE user_group_role CASCADE")
    con.commit()
    con.close()


def revert_migrate_user_group():
    con = psycopg2.connect(database=os.getenv("DB_NAME"), user=os.getenv("DB_USERNAME"),
                           password=os.getenv("DB_PASSWORD"), host=os.getenv("DB_HOST"),
                           port=os.getenv("DB_PORT"))

    cur = con.cursor()
    cur.execute("TRUNCATE user_group")
    con.commit()
    con.close()


def revert_migrate_chan_group():
    con = psycopg2.connect(database=os.getenv("DB_NAME"), user=os.getenv("DB_USERNAME"),
                           password=os.getenv("DB_PASSWORD"), host=os.getenv("DB_HOST"),
                           port=os.getenv("DB_PORT"))

    cur = con.cursor()
    cur.execute("TRUNCATE chan_group")
    con.commit()
    con.close()


def revert_migrate_tracks():
    con = psycopg2.connect(database=os.getenv("DB_NAME"), user=os.getenv("DB_USERNAME"),
                           password=os.getenv("DB_PASSWORD"), host=os.getenv("DB_HOST"),
                           port=os.getenv("DB_PORT"))

    cur = con.cursor()
    cur.execute("TRUNCATE tracks CASCADE")
    con.commit()
    con.close()


def revert_migrate_votes():
    con = psycopg2.connect(database=os.getenv("DB_NAME"), user=os.getenv("DB_USERNAME"),
                           password=os.getenv("DB_PASSWORD"), host=os.getenv("DB_HOST"),
                           port=os.getenv("DB_PORT"))

    cur = con.cursor()
    cur.execute("TRUNCATE votes")
    con.commit()
    con.close()


def __init__():
    load_dotenv()


def __migrate__():
    migrations = [migrate_groups, migrate_users, migrate_roles, migrate_user_group_role, migrate_user_group, migrate_chan_group, migrate_tracks, migrate_votes]
    for func in migrations:
        func()


def __revert__():
    migrations = [revert_migrate_groups, revert_migrate_users, revert_migrate_roles, revert_migrate_user_group_role, revert_migrate_user_group, revert_migrate_chan_group, revert_migrate_tracks, revert_migrate_votes]
    for func in migrations:
        func()


if __name__ == "__main__":
    __init__()
    try:
        __migrate__()
    finally:
        __revert__()
