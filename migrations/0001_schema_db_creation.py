import os
from dotenv import load_dotenv

import psycopg2
from psycopg2 import sql
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT


def create_database():
    con = psycopg2.connect(database='postgres', user=os.getenv("DB_USERNAME"),
                           password=os.getenv("DB_PASSWORD"), host=os.getenv("DB_HOST"),
                           port=os.getenv("DB_PORT"))

    con.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
    cur = con.cursor()

    cur.execute(sql.SQL("CREATE DATABASE {}")
                   .format(sql.Identifier(os.getenv("DB_NAME"))))

    con.commit()
    con.close()

    con = psycopg2.connect(database=os.getenv("DB_NAME"), user=os.getenv("DB_USERNAME"),
                           password=os.getenv("DB_PASSWORD"), host=os.getenv("DB_HOST"),
                           port=os.getenv("DB_PORT"))

    con.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
    cur = con.cursor()

    cur.execute("CREATE TABLE IF NOT EXISTS groups (\
            id SERIAL PRIMARY KEY,\
            group_code TEXT,\
            group_name TEXT,\
            group_playlist_url TEXT,\
            group_playlist_id TEXT,\
            group_threshold INTEGER DEFAULT 0)")
    cur.execute("CREATE TABLE IF NOT EXISTS users (\
            id SERIAL PRIMARY KEY,\
            user_tgid TEXT,\
            user_active_group INTEGER REFERENCES groups(id),\
            user_lang INTEGER DEFAULT 0)")
    cur.execute("CREATE TABLE IF NOT EXISTS roles (\
            id SERIAL PRIMARY KEY,\
            name TEXT,\
            description TEXT)")
    cur.execute("CREATE TABLE IF NOT EXISTS user_group_role (\
            id SERIAL PRIMARY KEY,\
            user_id INTEGER REFERENCES users(id),\
            group_id INTEGER REFERENCES groups(id),\
            role_id INTEGER REFERENCES roles(id))")
    cur.execute("CREATE TABLE IF NOT EXISTS user_group (\
            id SERIAL PRIMARY KEY,\
            group_id INTEGER REFERENCES groups(id),\
            user_id INTEGER REFERENCES users(id))")
    cur.execute("CREATE TABLE IF NOT EXISTS chan_group (\
            id SERIAL PRIMARY KEY,\
            group_id INTEGER REFERENCES groups(id),\
            chan_tgid TEXT)")
    cur.execute("CREATE TABLE IF NOT EXISTS tracks (\
            id SERIAL PRIMARY KEY,\
            group_id INTEGER REFERENCES groups(id),\
            user_id INTEGER REFERENCES users(id),\
            track_url TEXT,\
            votes_for INTEGER DEFAULT 0,\
            votes_against INTEGER DEFAULT 0,\
            action INTEGER,\
            date TIMESTAMPTZ,\
            dead BOOLEAN DEFAULT FALSE)")  # action : 0 for removing 1 for adding
    cur.execute("CREATE TABLE IF NOT EXISTS votes (\
            id SERIAL PRIMARY KEY,\
            user_id INTEGER REFERENCES users(id),\
            track_id INTEGER REFERENCES tracks(id),\
            vote INTEGER,\
            date TIMESTAMPTZ)")
    con.commit()
    con.close()


def revert_create_database():
    con = psycopg2.connect(database='postgres', user=os.getenv("DB_USERNAME"),
                           password=os.getenv("DB_PASSWORD"), host=os.getenv("DB_HOST"),
                           port=os.getenv("DB_PORT"))

    con.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)

    cur = con.cursor()

    cur.execute(sql.SQL("DROP DATABASE {}").format(
        sql.Identifier(os.getenv("DB_NAME")))
    )
    con.commit()
    con.close()


def __init__():
    load_dotenv()


def __migrate__():
    create_database()


def __revert__():
    revert_create_database()


if __name__ == "__main__":
    __init__()
    try:
        __migrate__()
    finally:
        # print("No revert")
        __revert__()
