import os

from dotenv import load_dotenv

import psycopg2


def migrate_roles():
    con = psycopg2.connect(database=os.getenv("DB_NAME"), user=os.getenv("DB_USERNAME"),
                           password=os.getenv("DB_PASSWORD"), host=os.getenv("DB_HOST"),
                           port=os.getenv("DB_PORT"))
    cur = con.cursor()
    cur.execute("INSERT INTO roles (name, description) VALUES ('owner', 'The owner is the person that created the group. It is bound to their Spotify account'), ('regular', 'Regular has no privilege')")
    cur.execute("SELECT setval('roles_id_seq', (SELECT MAX(id) FROM roles)+1)")
    con.commit()
    con.close()


def migrate_user_group_role():
    db = psycopg2.connect(database=os.getenv("DB_NAME"), user=os.getenv("DB_USERNAME"),
                          password=os.getenv("DB_PASSWORD"), host=os.getenv("DB_HOST"),
                          port=os.getenv("DB_PORT"))

    cur = db.cursor()
    cur.execute("SELECT id FROM roles WHERE name='admin'")
    admin_role = cur.fetchone()[0]
    cur.execute("SELECT id FROM roles WHERE name='owner'")
    owner_role = cur.fetchone()[0]
    cur.execute("SELECT id FROM roles WHERE name='regular'")
    regular_role = cur.fetchone()[0]

    cur.execute("SELECT * FROM user_group")
    ugs = cur.fetchall()

    for ug in ugs:
        cur.execute("SELECT COUNT(*) FROM user_group_role WHERE user_id=%s AND group_id=%s AND role_id=%s", (ug[2], ug[1], admin_role))
        count = cur.fetchone()[0]
        if count:
            cur.execute("UPDATE user_group_role SET role_id=%s WHERE user_id=%s AND group_id=%s", (owner_role, ug[2], ug[1]))
        else:
            cur.execute("INSERT INTO user_group_role (user_id, group_id, role_id) VALUES (%s, %s, %s)", (ug[2], ug[1], regular_role))
    cur.execute("SELECT setval('user_group_role_id_seq', (SELECT MAX(id) FROM user_group_role)+1)")

    db.commit()
    db.close()


# Revert


def revert_migrate_roles():
    con = psycopg2.connect(database=os.getenv("DB_NAME"), user=os.getenv("DB_USERNAME"),
                           password=os.getenv("DB_PASSWORD"), host=os.getenv("DB_HOST"),
                           port=os.getenv("DB_PORT"))

    cur = con.cursor()
    cur.execute("DELETE FROM roles WHERE name='owner' OR name='regular'")
    cur.execute("SELECT setval('roles_id_seq', (SELECT MAX(id) FROM roles)+1)")
    con.commit()
    con.close()


def revert_migrate_user_group_role():
    con = psycopg2.connect(database=os.getenv("DB_NAME"), user=os.getenv("DB_USERNAME"),
                           password=os.getenv("DB_PASSWORD"), host=os.getenv("DB_HOST"),
                           port=os.getenv("DB_PORT"))

    cur = con.cursor()
    cur.execute("SELECT id FROM roles WHERE name='admin'")
    admin_role = cur.fetchone()[0]
    cur.execute("SELECT id FROM roles WHERE name='owner'")
    owner_role = cur.fetchone()[0]

    cur.execute("SELECT * FROM user_group_role")
    ugrs = cur.fetchall()

    for ugr in ugrs:
        if ugr[3] == owner_role:
            cur.execute("UPDATE user_group_role SET role_id=%s WHERE user_id=%s AND group_id=%s", (admin_role, ugr[1], ugr[2]))
        else:
            cur.execute("DELETE FROM user_group_role WHERE id=%s", (ugr[0],))
    cur.execute("SELECT setval('user_group_role_id_seq', (SELECT MAX(id) FROM user_group_role)+1)")
    con.commit()
    con.close()


def __init__():
    load_dotenv()


def __migrate__():
    migrations = [migrate_roles, migrate_user_group_role]
    for func in migrations:
        func()


def __revert__():
    migrations = [revert_migrate_user_group_role, revert_migrate_roles]
    for func in migrations:
        func()


if __name__ == "__main__":
    __init__()
    try:
        __migrate__()
    finally:
        __revert__()
