# Contributing

You can contribute to this project in two ways :

# I18n

  To translate the bot to a language of your choice follow this procedure :
  - Copy the template file in the lang folder and rename it following the
  naming convention of the other files in the folder (`lang_LANG_VERSION.json`)
  - Translate as many strings as you can and set "translated" to true on each
  translated string. Keep the non-translated strings to english, they will act
  as default values.
  - Set the flag_emoji value. See __finding the flag emoji__.
  - Set the id to max(id) + 1.
  - Create a push request.

In the context of languages files, _version_ refers to the version of the list
of strings. When the app evolve, more string are added and the app expects to
find all the strings it needs in the languages files. As such it'll only load
the files of the newest version.  

A translation is considered 'usable' when more than 90% of the strings are
translated. If less than 90% of the strings are translated, thelang file will be
kept in the versioning system but not loaded by the app.

## Finding the Flag Emoji

  It is crucial that you use a json compatible enconding of the flag emoji. One
  safe way to do it is to follow this procedure :
  - Find your emoji codepoint(s) using emojipedia.org. Many flags have two 
  codepoints, so follow the procedure for each codepoint and assemble at the
  end. Example : French Flag → U+1F1EB and U+1F1F7.
  - Go to codepoints.net and use the search function to find each codepoint.
  - On the returned page, go to Representations > Show More and copy the
  _JavaScript, JSON and Java_ value. Example U+1F1EB → \uD83C\uDDEB and 
  U+1F1F7 → \uD83C\uDDF7.
  - Assemble and paste in your json file.
  Example : French flag → \uD83C\uDDEB\uD83C\uDDF7.

# Code
To contribute to the code :
- Clone the repository
- Set up a testing bot with @BotFather
- Set up a running PostgreSQL database
- Copy the .env template to .env and fill the file with your values (you can get
your telegram id by logging message.from_user.id on /start)
- Pick an issue and solve it.
- Run pylama (skip your env folder).
- Create a push request.

# Suggesting features or reporting bugs
Create new issues on this gitlab repository.


Thank you for your Help !
