import os
import json


class LangManager(object):
    def __init__(self):
        super().__init__()
        self.langs = {}
        self.load_langs()
        self.check_langs()

    def load_langs(self):
        for file in os.listdir("langs/"):
            if file != "lang_template.json":
                with open(f"langs/{file}", "r") as f:
                    lang_json = json.load(f)
                    if lang_json['id'] not in self.langs:
                        self.langs[lang_json['id']] = lang_json
                    elif self.langs[lang_json['id']]['lang_intl'] == lang_json['lang_intl']:
                        if self.langs[lang_json['id']]['version'] < lang_json['version']:
                            self.langs[lang_json['id']] = lang_json
                        elif self.langs[lang_json['id']]['version'] == lang_json['version']:
                            print(f"Error: Lang of id {lang_json['id']} and name {lang_json['lang_intl']} is present twice with the same version.")
                            print("E Ignoring second occurence")
                    else:
                        print(f"Error: Lang of id {lang_json['id']} is present twice with names {lang_json['lang_intl']} and {self.langs[lang_json['id']]['lang_intl']}.")
                        print("E Ignoring second occurence")

    def check_langs(self):
        versions = []
        string_lengths = []
        for id in self.langs:
            versions.append(self.langs[id]['version'])
            string_lengths.append(len(self.langs[id]['strings'].keys()))
        assert len(list(set(versions))) == 1
        assert len(list(set(string_lengths))) == 1
        print(f"{len(versions)} languages loaded for {string_lengths[0]} strings. Current version : {versions[0]}")
        for id in self.langs:
            trs = 0
            for string in self.langs[id]['strings']:
                if self.langs[id]['strings'][string]['translated']:
                    trs += 1
            percent_value = int(trs/string_lengths[0]*100)
            print(f"{self.langs[id]['lang_intl']} - {percent_value}% completed")
            if percent_value < 90:
                print(f"{self.langs[id]['lang_intl']} is under 90% completion. Dropping")
                self.langs.pop(id)

    def get_emoji(self, lang):
        return self.langs[lang]['flag_emoji']

    def get_emojis(self):
        for id in self.langs:
            yield (id, self.langs[id]['flag_emoji'])

    def get_string(self, key, lang):
        return self.langs[lang]['strings'][key]['string']


if __name__ == "__main__":
    lm = LangManager()
