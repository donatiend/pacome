import unittest
import os
from dotenv import load_dotenv

from spotify import SpotifyAPI
from database import PacomeDB


class TestSpotifyAPI(unittest.TestCase):

    @classmethod
    def setUpClass(self):
        load_dotenv()
        self.spApi = SpotifyAPI()
        self.spApi.login()
        self.playlist_id = None

    def test_playlist_scenario(self):
        playlist_url, self.playlist_id, snapshot = self.spApi.createPlaylist(os.getenv("TEST_PLAYLIST"))
        self.assertTrue(playlist_url.startswith('https://open.spotify.com/playlist/'))
        _ = self.spApi.addTracks(self.playlist_id, ["https://open.spotify.com/track/4N2Z0No2oTM1waCclhfOVL?si=A0dgDO0dSs6LOTBWz9GoFQ", "spotify:track:1npqP0Epwvvc1jzudkkmVB", "https://open.spotify.com/album/7bMpvjE9r9KDv2prNYaVpg?si=0yfZhkShRqyXVc_JVHwqLQ"])
        _ = self.spApi.removeTracks(self.playlist_id, ["https://open.spotify.com/track/4N2Z0No2oTM1waCclhfOVL?si=A0dgDO0dSs6LOTBWz9GoFQ", "spotify:track:1npqP0Epwvvc1jzudkkmVB"])

    def test_getRandomTrackFromAlbum(self):
        track_uri = self.spApi._getRandomTrackFromAlbum("https://open.spotify.com/album/7bMpvjE9r9KDv2prNYaVpg?si=0yfZhkShRqyXVc_JVHwqLQ")
        self.assertTrue(track_uri.startswith("spotify:track:"))
        track_uri = self.spApi._getRandomTrackFromAlbum("spotify:album:2w4tLG1JH25BysuILfkI0x")
        self.assertTrue(track_uri.startswith("spotify:track:"))

    def test_getUserId(self):
        self.spApi._getUserId()
        self.assertEqual(self.spApi.user_id, os.getenv("SPOTIFY_OWNER_ID"))

    def test_parseTrack(self):
        track_uri = self.spApi.parseTrack("https://open.spotify.com/track/4N2Z0No2oTM1waCclhfOVL?si=A0dgDO0dSs6LOTBWz9GoFQ")
        self.assertEqual(track_uri, "spotify:track:4N2Z0No2oTM1waCclhfOVL")
        track_uri = self.spApi.parseTrack("spotify:track:1npqP0Epwvvc1jzudkkmVB")
        self.assertEqual(track_uri, "spotify:track:1npqP0Epwvvc1jzudkkmVB")
        track_uri = self.spApi.parseTrack("https://open.spotify.com/album/7bMpvjE9r9KDv2prNYaVpg?si=0yfZhkShRqyXVc_JVHwqLQ")
        self.assertTrue(track_uri.startswith("spotify:track:"))

    def test_parseTracks(self):
        tracks_uri = self.spApi.parseTracks(["https://open.spotify.com/track/4N2Z0No2oTM1waCclhfOVL?si=A0dgDO0dSs6LOTBWz9GoFQ", "spotify:track:1npqP0Epwvvc1jzudkkmVB"])
        self.assertEqual(tracks_uri, ["spotify:track:4N2Z0No2oTM1waCclhfOVL", "spotify:track:1npqP0Epwvvc1jzudkkmVB"])

    def test_uriFromUrl(self):
        uri = self.spApi._uriFromUrl("https://open.spotify.com/track/4N2Z0No2oTM1waCclhfOVL?si=A0dgDO0dSs6LOTBWz9GoFQ")
        self.assertEqual(uri, "spotify:track:4N2Z0No2oTM1waCclhfOVL")

    def test_refreshToken(self):
        token = self.spApi.token
        token_type = self.spApi.token_type
        refresh_token = self.spApi.refresh_token
        token_birth = self.spApi.token_birth
        code = self.spApi.code
        self.spApi._refresh_token()
        self.assertEqual(code, self.spApi.code)
        self.assertEqual(refresh_token, self.spApi.refresh_token)
        self.assertEqual(token_type, self.spApi.token_type)
        self.assertFalse(token == self.spApi.token)
        self.assertTrue(token_birth < self.spApi.token_birth)

    def test_base64encode(self):
        b64str = self.spApi._base64encode('azeE3f4')
        self.assertEqual(b64str, 'YXplRTNmNA==')

    # def test_noTokenException(self):
    #     self.assertRaises(NoTokenError, SpotifyAPI().createPlaylist('aaaaaa'))
    #     self.assertRaises(NoTokenError, SpotifyAPI().deletePlaylist('aaaaaa'))
    #     self.assertRaises(NoTokenError, SpotifyAPI().addTracks(['aaaaaa']))
    #     self.assertRaises(NoTokenError, SpotifyAPI().removeTracks(['aaaaaa']))
    #     self.assertRaises(NoTokenError, SpotifyAPI()._getRandomTrackFromAlbum('aaaaaa'))

    # def test_BadIdentifier(self):
    #     self.assertRaises(BadIdentifier, self.spApi._getRandomTrackFromAlbum('aaaaaa'))

    @classmethod
    def tearDownClass(self):
        if self.playlist_id:
            self.spApi.deletePlaylist(self.playlist_id)


class TestPacomeDB(unittest.TestCase):

    def setUp(self):
        print("SetUp")
        load_dotenv()
        self.db = PacomeDB(os.getenv("TEST_DB_NAME"))

    def test_getGroupCode(self):
        print("test_getGroupCode")
        self.assertEqual(len(self.db._getGroupCode()), int(os.getenv("ID_SIZE")))

    def test_track_scenario(self):
        print("test_track_scenario")
        track_url = 'https://open.spotify.com/track/5nwhi8NkCnT02kNgO2NBBn'
        playlist_url = "playlist_url"
        playlist_id = "playlist_id"
        action = 0
        a_tgid = 'aaaaaa'
        a_id = self.db.registerUser(a_tgid)
        group_id, group_name, group_code, playlist_url = self.db.createGroup(os.getenv("TEST_PLAYLIST"), a_id, playlist_url, playlist_id, 2)
        track_id = self.db.createTrack(group_id, a_id, track_url, action)
        track1 = self.db.getTrack(id=track_id)[0]
        self.db.updateTrack(track_id, action=1)
        track2 = self.db.getTrack(id=track_id)[0]
        self.assertEqual(track2[0], track1[0])
        self.assertEqual(track2[1], track1[1])
        self.assertEqual(track2[2], track1[2])
        self.assertEqual(track2[3], track1[3])
        self.assertEqual(track2[4], track1[4])
        self.assertEqual(track2[5], track1[5])
        self.assertEqual(track2[6], 1)
        self.assertEqual(track2[7], track1[7])
        self.assertEqual(track2[8], track1[8])
        tracks = self.db.getTrack()
        self.assertEqual(len(tracks), 1)
        track_id2 = self.db.getTrack('id')[0][0]
        self.assertEqual(track_id2, track_id)

    def test_users_group_scenario(self):
        print("test_user_group_scenario")
        playlist_url = "playlist_url"
        playlist_id = "playlist_id"
        a_tgid = 'aaaaaa'
        b_tgid = 'bbbbbb'
        c_tgid = 'cccccc'
        a = self.db.registerUser(a_tgid)
        b = self.db.registerUser(b_tgid)
        a_id = self.db.getUserId(a_tgid)
        b_id = self.db.getUserId(b_tgid)
        self.assertEqual(a_id, a)
        self.assertEqual(b_id, b)
        self.assertEqual(self.db.getUserLang(a_tgid), 0)
        self.assertEqual(self.db.getUserLang(b_tgid), 0)
        group_id, group_name1, group_code, playlist_url1 = self.db.createGroup(os.getenv("TEST_PLAYLIST"), a_id, playlist_url, playlist_id, 2)
        self.assertFalse(self.db.isMember(a_id, group_id))
        self.assertEqual(self.db.join(group_code, a_id), (group_name1, playlist_url1))
        self.assertTrue(self.db.isMember(a_id, group_id))
        self.assertEqual(group_name1, os.getenv("TEST_PLAYLIST"))
        self.assertEqual(playlist_url1, playlist_url)
        group_name2, playlist_url2 = self.db.join(group_code, b_id)
        self.assertEqual(group_name1, group_name2)
        self.assertEqual(playlist_url1, playlist_url2)
        playlist_id1 = self.db.getUserActivePlaylist(a_tgid)
        playlist_id2 = self.db.getUserActivePlaylist(b_tgid)
        self.assertEqual(playlist_id1, playlist_id2)
        self.assertEqual(playlist_id1, playlist_id)

        self.db.updateUser(b_id, user_lang=1, user_tgId=c_tgid)
        self.assertEqual(self.db.getUserId(c_tgid), b_id)
        self.assertEqual(self.db.getUserLang(c_tgid), 1)
        self.assertEqual(self.db.getUserActivePlaylist(c_tgid), playlist_id1)

        _ = self.db.getUserGroups(a_id)
        self.assertEqual(len(self.db.getAllUsers()), 2)

    def test_vote_scenario(self):
        print("test_vote_scenario")
        playlist_url = "playlist_url2"
        playlist_id = "playlist_id2"
        a_tgid = 'aaaa'
        b_tgid = 'bbbb'

        a_id = self.db.registerUser(a_tgid)
        b_id = self.db.registerUser(b_tgid)

        group_id, group_name, group_code, playlist_url = self.db.createGroup(os.getenv("TEST_PLAYLIST"), a_id, playlist_url, playlist_id, 2)
        _, _ = self.db.join(group_code, a_id)
        _, _ = self.db.join(group_code, b_id)

        track_url = 'https://open.spotify.com/track/5nwhi8NkCnT02kNgO2NBBn'
        action = 0
        track_id = self.db.createTrack(group_id, a_id, track_url, action)
        _ = self.db.createVote(a_id, track_id, 1)
        self.assertTrue(self.db.hasVoted(a_id, track_id))
        self.assertFalse(self.db.hasVoted(b_id, track_id))

    def test_binding_scenario(self):
        print("test_binding_scenario")
        playlist_url = "playlist_url2"
        playlist_id = "playlist_id2"
        a_tgid = 'aaa'
        chan = 'asdfldsnf'

        a_id = self.db.registerUser(a_tgid)

        group_id, group_name, group_code, playlist_url = self.db.createGroup(os.getenv("TEST_PLAYLIST"), a_id, playlist_url, playlist_id, 2)
        _, _ = self.db.join(group_code, a_id)

        self.db.createBinding(chan, group_id)
        self.assertEqual(self.db.getChanBinding(chan), group_id)

    def tearDown(self):
        self.db.delete()

# class TestPlaylistManager(unittest.TestCase):
#
#     @classmethod
#     def setUpClass(self):
#         load_dotenv()
#         db = PacomeDB(os.getenv("TEST_DB_NAME"))
#         self.pm = PlaylistManager(db)
#         self.pm.login()
#
#     def test_track_scenario(self):
#         track = 'https://open.spotify.com/track/5nwhi8NkCnT02kNgO2NBBn'
#         tracks = ["https://open.spotify.com/track/4N2Z0No2oTM1waCclhfOVL", "spotify:track:1npqP0Epwvvc1jzudkkmVB"]
#         url, sp_id, snapshot = self.pm.createPlaylist(os.getenv("TEST_PLAYLIST"))
#         self.pm._addTrackSpotify(track, playlist_id)
#         self.pm._addTracksSpotify(tracks, playlist_id)
#         self.pm._removeTrackSpotify(track, playlist_id)
#         self.pm._removeTracksSpotify(tracks, playlist_id)
#         self.pm.vote(user_id, track_id, vote)
#         self.pm._trackUpdate()


if __name__ == '__main__':
    unittest.main()
