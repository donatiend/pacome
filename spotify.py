import os
import requests
import urllib.parse
from gevent.pywsgi import WSGIServer
from flask import Flask, request, render_template
import random
import string
import base64
from datetime import datetime, timedelta
import functools
import time
from tzlocal import get_localzone
import pytz


class BadIdentifier(Exception):
    """Exception raised for inexistant playlists.

    Attributes:
        expression -- input expression in which the error occurred
        message -- explanation of the error
    """

    def __init__(self, expression, message):
        self.expression = expression
        self.message = message


class NoTokenError(Exception):
    """Exception raised for inexistant playlists.

    Attributes:
        expression -- input expression in which the error occurred
        message -- explanation of the error
    """

    def __init__(self, expression, message):
        self.expression = expression
        self.message = message


class SpotifyServer:
    def __init__(self):
        self.isRunning = False
        self.host = os.getenv('HOST')
        self.port = int(os.getenv('PORT'))
        self.http_server = None
        self.app = Flask(__name__)
        self.token = None
        self.token_type = None
        self.refresh_token = None
        self.expire_time = None
        self.token_birth = None

        # # Rather ugly way to get the id token. Must be a better way

        # self.token = ""
        # self.token_type = ""
        # self.refresh_token = ""
        # self.token_birth = None
        # self.user_id = ""
        # self.code = ""

        # # Disable flask output
        # log = logging.getLogger('werkzeug')
        # log.setLevel(logging.ERROR)
        # cli = sys.modules['flask.cli']
        # cli.show_server_banner = lambda *x: None

        @self.app.route('/<state>')
        def index(state):
            params = {
                    "client_id": os.getenv("SPOTIFY_CLIENT_ID"),
                    "response_type": "code",
                    "redirect_uri": f"http://{os.getenv('HOST')}:{os.getenv('PORT')}/callback",
                    "scope": "playlist-modify-public ugc-image-upload",
                    "state": state
                    }
            url = "https://accounts.spotify.com/authorize?" + urllib.parse.urlencode(params)
            return render_template("login.html", redirect_url=url)

        @self.app.route('/callback')
        def callback():
            code = request.args.get('code')
            state = request.args.get('state')
            if state == self.state:
                header_string = f"{os.getenv('SPOTIFY_CLIENT_ID')}:{os.getenv('SPOTIFY_CLIENT_SECRET')}"
                payload = {
                        "grant_type": 'authorization_code',
                        "code": code,
                        "redirect_uri": f"http://{os.getenv('HOST')}:{os.getenv('PORT')}/callback",
                        }
                header = {
                        "Authorization": f"Basic {self._base64encode(header_string)}"
                        }
                r = requests.post("https://accounts.spotify.com/api/token", headers=header, data=payload)
                if r.status_code == 200:
                    rjson = r.json()
                    self.token = rjson['access_token']
                    self.token_type = rjson['token_type']
                    self.refresh_token = rjson['refresh_token']
                    self.expire_time = rjson['expires_in']
                    local_tz = get_localzone()
                    self.token_birth = datetime.now().replace(tzinfo=pytz.utc).astimezone(local_tz)
                    self.http_server.stop()
                    return "You are logged in. You can close this window."
            self.http_server.stop()
            return "There has been an error. If you are sure about your credentials, please contact the administrator"

    def _base64encode(self, message):
        message_bytes = message.encode('ascii')
        base64_bytes = base64.b64encode(message_bytes)
        base64_message = base64_bytes.decode('ascii')
        return base64_message

    def start(self, state, count=0):
        if count > 10:
            print('Timeout')
            return
        if self.http_server is None:
            self.state = state
            self.http_server = WSGIServer((self.host, self.port), self.app)
            self.http_server.serve_forever()
        else:
            time.sleep(2)
            self.start(state, count + 1)
        return self._get_token_data()

    def _get_token_data(self):
        td = {}
        td['token'] = self.token
        td['token_type'] = self.token_type
        td['refresh_token'] = self.refresh_token
        td['expire_time'] = self.expire_time
        td['token_birth'] = self.token_birth

        return td


class SpotifyAPI(object):
    # TODO Search album by name/artist
    # TODO Search track by name/artist

    def __init__(self, db):
        super().__init__()
        self.server = SpotifyServer()
        self.db = db

    def authorized_request(func):
        @functools.wraps(func)
        def wrapper(self, group_id, *args, **kwargs):
            token_data = self.db.getSpAuth(group_id)
            if not token_data or token_data['token'] == "" or token_data['token_type'] == "":
                raise NoTokenError("Cannot send request without authorization token", "Check if you are logged in")
            if 'sp_user_id' not in token_data or token_data['sp_user_id'] == "":
                _, token_data['sp_user_id'] = self._getUserId(token_data)
            token_data = self._check_token(token_data)
            return token_data, func(self, group_id, token_data['sp_user_id'], token_data['token_type'], token_data['token'], *args, **kwargs)
        return wrapper

    def _base64encode(self, message):
        message_bytes = message.encode('ascii')
        base64_bytes = base64.b64encode(message_bytes)
        base64_message = base64_bytes.decode('ascii')
        return base64_message

    def _check_token(self, token_data):
        if token_data['token']:
            local_tz = get_localzone()
            tz_local_now = datetime.now().replace(tzinfo=pytz.utc).astimezone(local_tz)
            if tz_local_now > token_data['token_birth'] + timedelta(seconds=int(token_data['expire_time'])):
                token_data = self._refresh_token(token_data)
                self.db.updateSpAuth(token_data)
        return token_data

    def _refresh_token(self, token_data):
        header_string = f"{os.getenv('SPOTIFY_CLIENT_ID')}:{os.getenv('SPOTIFY_CLIENT_SECRET')}"
        header = {
                "Authorization": f"Basic {self._base64encode(header_string)}"
                }
        payload = {
                'grant_type': 'refresh_token',
                'refresh_token': token_data['refresh_token']
                }
        r = requests.post("https://accounts.spotify.com/api/token", data=payload, headers=header)
        if r.status_code == 200:
            rjson = r.json()
            token_data['token'] = rjson['access_token']
            token_data['token_type'] = rjson['token_type']
            token_data['expire_time'] = rjson['expires_in']
            token_data['token_birth'] = datetime.now()
        return token_data

    # Util
    def _uriFromUrl(self, url):
        return url.split("?")[0].replace("https://open.spotify.com/", "spotify:").replace("/", ":")

    # Util
    def parseTrack(self, group_id, track):
        if track.startswith("https://open.spotify.com/track"):  # Spotify track url
            return self._uriFromUrl(track)
        elif track.startswith("spotify:track"):  # Spotify track uri
            return track
        elif track.startswith("https://open.spotify.com/album") or track.startswith("spotify:album"):  # Spotify track url or uri
            return self._getRandomTrackFromAlbum(group_id, track)[1]
        else:
            raise BadIdentifier("Cannot parse track", "Make sure it is a spotify URI or URL")

    # Util
    def parseTracks(self, group_id, tracks):
        return list(map(lambda x: self.parseTrack(group_id, x), tracks))

    def login(self, user_id):
        state = ''.join(random.SystemRandom().choice(string.ascii_lowercase) for _ in range(10))
        url = f"http://{os.getenv('HOST')}:{os.getenv('PORT')}/{state}"
        yield url
        token_data = self.server.start(state)
        token_data['sp_display_name'], token_data['sp_user_id'] = self._getUserId(token_data)
        token_data['user_id'] = user_id
        token_data['id'] = self.db.createSpAuth(token_data)
        yield token_data

    def _getUserId(self, token_data):
        self._check_token(token_data)
        headers = {"Authorization": f"{token_data['token_type']} {token_data['token']}"}
        r = requests.get("https://api.spotify.com/v1/me", headers=headers)
        rjson = r.json()
        return rjson['display_name'], rjson['id']

    @authorized_request
    def createPlaylist(self, group_id, sp_user_id, token_type, token, name):
        data = {"name": name}
        headers = {"Authorization": f"{token_type} {token}", "Content-Type": "application/json"}
        r = requests.post(f"https://api.spotify.com/v1/users/{sp_user_id}/playlists", json=data, headers=headers)
        rjson = r.json()
        return rjson['external_urls']['spotify'], rjson['id'], rjson['snapshot_id']

    @authorized_request
    def deletePlaylist(self, group_id, sp_user_id, token_type, token, playlist_id):
        headers = {"Authorization": f"{token_type} {token}"}
        _ = requests.delete(f"https://api.spotify.com/v1/playlists/{playlist_id}/followers", headers=headers)
        # TODO When playlist doesn't exist anymore
        # assert r.status_code == 200

    @authorized_request
    def uploadCoverPicture(self, group_id, sp_user_id, token_type, token, playlist_id, picture):
        headers = {"Authorization": f"{token_type} {token}", "Content-Type": "image/jpeg"}
        r = requests.put(f"https://api.spotify.com/v1/playlists/{playlist_id}/images", headers=headers, data=picture)
        assert r.status_code == 200 or r.status_code == 202

    @authorized_request
    def uploadDescription(self, group_id, sp_user_id, token_type, token, playlist_id, description):
        headers = {"Authorization": f"{token_type} {token}"}
        data = {"description": description}
        r = requests.put(f"https://api.spotify.com/v1/playlists/{playlist_id}", headers=headers, json=data)
        assert r.status_code == 200 or r.status_code == 202

    @authorized_request
    def addTracks(self, group_id, sp_user_id, token_type, token, playlist_id, tracks):
        headers = {"Authorization": f"{token_type} {token}", "Content-Type": "application/json"}
        data = {"uris": self.parseTracks(group_id, tracks)}
        r = requests.post(f"https://api.spotify.com/v1/playlists/{playlist_id}/tracks", json=data, headers=headers)
        # returns 404 when playlist doesn't exist
        return r.json()['snapshot_id']

    @authorized_request
    def removeTracks(self, group_id, sp_user_id, token_type, token, playlist_id, tracks):
        headers = {"Authorization": f"{token_type} {token}", "Content-Type": "application/json"}
        data = {"tracks": [{"uri": value} for value in self.parseTracks(group_id, tracks)]}
        r = requests.delete(f"https://api.spotify.com/v1/playlists/{playlist_id}/tracks", json=data, headers=headers)
        return r.json()['snapshot_id']

    @authorized_request
    def _getRandomTrackFromAlbum(self, group_id, sp_user_id, token_type, token, album):
        if album.startswith("https://open.spotify.com/album"):  # Spotify album url
            album_uri = self._uriFromUrl(album)
        elif album.startswith("spotify:album"):  # Spotify album uri
            album_uri = album
        else:
            raise BadIdentifier("Could not find album with this identifier", "Check if you have a valid spotify URI or URL")
        album_id = album_uri.split(":")[-1]

        headers = {"Authorization": f"{token_type} {token}", "Content-Type": "application/json"}
        r = requests.get(f"https://api.spotify.com/v1/albums/{album_id}", headers=headers)

        rand_index = random.randrange(len(r.json()['tracks']['items']))
        return r.json()['tracks']['items'][rand_index]['uri']
