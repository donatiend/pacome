import telebot

from playlistManager import PlaylistManager
from langManager import LangManager
from database import PacomeDB

from Core.bot import Bot

from spotify import NoTokenError, BadIdentifier

lm = LangManager()
db = PacomeDB()
pm = PlaylistManager(db)

actions = ('removeTrack', 'addTrack')


# Broadcasts


def vote_bc(pacome, broadcast_data, user, group):
    if type(broadcast_data['track_id']) == list and len(broadcast_data['track_id']) > 1:
        for i, id in enumerate(broadcast_data['track_id']):
            pacome.trigger('vote', {'track': broadcast_data['track'][i], 'track_id': id, 'action': broadcast_data['action']}, user, group)
    else:
        if type(broadcast_data['track_id']) == list:
            broadcast_data['track_id'] = broadcast_data['track_id'][0]
            broadcast_data['track'] = broadcast_data['track'][0]
        markup = telebot.types.InlineKeyboardMarkup()
        itembtn1 = telebot.types.InlineKeyboardButton(u"\U0001F44D", callback_data=f"vote1:{broadcast_data['track_id']}")
        itembtn2 = telebot.types.InlineKeyboardButton(u"\U0001F44E", callback_data=f"vote0:{broadcast_data['track_id']}")
        markup.add(itembtn1, itembtn2)

        stringo = lm.get_string('vote', user.user_lang).format(group.group_name, actions[broadcast_data['action']], broadcast_data['track'])

        return stringo, markup


def announce_bc(pacome, broadcast_data, user, group):
    return broadcast_data['message'], None

# Commands


def announce(pacome, message, user):
    group = user.active_group
    pacome.trigger('announce', {'message': message.content}, user, group, parse_mode=None)
    pacome.send_message(message.from_user.id, lm.get_string('announcement_broadcasted', user.user_lang), parse_mode='MarkdownV2')


def login(pacome, message, user):
    a = pm.login(user.id)
    if a:
        url = next(a)
        pacome.send_message(message.from_user.id, lm.get_string('login_sp_url', user.user_lang).format(url), parse_mode='MarkdownV2')
        token_data = next(a)
        assert token_data['user_id'] == user.id
        pacome.send_message(message.from_user.id, lm.get_string('login_sp_success', user.user_lang).format(token_data['sp_display_name']), parse_mode='MarkdownV2')
    else:
        pacome.send_message(message.from_user.id, lm.get_string('login_sp_already', user.user_lang), parse_mode='MarkdownV2')


def logout(pacome, message, user):
    db.delSpAuth(user.id)
    pacome.send_message(message.from_user.id, lm.get_string('logout_sp', user.user_lang), parse_mode='MarkdownV2')


def join(pacome, message, user):
    try:
        group, playlist = db.join(user.id, message.content)
        pacome.send_message(message.chat.id, lm.get_string('joined', user.user_lang).format(group, playlist), parse_mode='MarkdownV2')
    except PacomeDB.UnknownGroupError:
        pacome.send_message(message.chat.id, lm.get_string('unknown_group', user.user_lang), parse_mode='MarkdownV2')


def leave(pacome, message, user):
    if message.content:
        group_code = message.content
        group = pacome.get_group(group_code)
    else:
        group = user.active_group
    if pacome.check_role(user, group, ['owner']):
        pacome.send_message(message.chat.id, lm.get_string('not_authorized', user.user_lang), parse_mode='MarkdownV2')
    else:
        try:
            _ = db.leave(user.id, group.group_code)
            pacome.send_message(message.chat.id, lm.get_string('left', user.user_lang).format(group.group_name), parse_mode='MarkdownV2')
        except PacomeDB.UnknownGroupError:
            pacome.send_message(message.chat.id, lm.get_string('unknown_group', user.user_lang), parse_mode='MarkdownV2')


def updateCover(pacome, message, user, base64_photo):
    try:
        pm.uploadCover(base64_photo, user.active_group.id)
        pacome.send_message(message.chat.id, lm.get_string('cover_updated', user.user_lang), parse_mode='MarkdownV2')
    except NoTokenError:
        pacome.send_message(message.from_user.id, lm.get_string('no_login_sp', user.user_lang), parse_mode='MarkdownV2')


def updateDesc(pacome, message, user):
    pm.uploadDesc(message.content, user.active_group.id)
    pacome.send_message(message.chat.id, lm.get_string('desc_updated', user.user_lang), parse_mode='MarkdownV2')


def updateThresh(pacome, message, user):
    group = user.active_group
    try:
        threshold = int(message.content)
        db.updateGroup(group.id, group_threshold=threshold)
        # Make group threshold a parameter
        pacome.send_message(message.chat.id, lm.get_string('thresh_updated', user.user_lang).format(group.group_name), parse_mode='MarkdownV2')
    except ValueError:
        pacome.send_message(message.chat.id, lm.get_string('not_int', user.user_lang), parse_mode='MarkdownV2')


# def update(pacome, message, user):
#     if not message.content:
#         group = user.active_group
#     else:
#         group = pacome.get_group(message.content)
#     markup = telebot.types.InlineKeyboardMarkup()
#     itembtn = telebot.types.InlineKeyboardButton("description",
#                                                  callback_data=f"update:{group.id}:0")
#     itembtn = telebot.types.InlineKeyboardButton("threshold",
#                                                  callback_data=f"update:{group.id}:1")
#     itembtn = telebot.types.InlineKeyboardButton("cover",
#                                                  callback_data=f"update:{group.id}:2")
#     markup.add(itembtn)
#     pacome.send_message(message.from_user.id, lm.get_string('update_group', user.user_lang), reply_markup=markup)


def info(pacome, message, user):
    if not message.content:
        group = user.active_group
    else:
        group = pacome.get_group(message.content)
    members = pacome.get_members(group)
    pending_tracks = pm.get_pending(group)
    pacome.send_message(message.from_user.id,
                        lm.get_string('info_group', user.user_lang).format(
                            group.group_name,
                            group.group_code,
                            group.group_playlist_url,
                            len(members),
                            len(pending_tracks),
                            group.group_threshold),
                        parse_mode='MarkdownV2')


def addTrack(pacome, message, user, group=None):
    # if admin addTrackAdmin
    try:
        if not group:
            group = user.active_group
        if group == -1 or group is None:
            raise IndexError  # TODO create custom error
        urls = [message.text[e.offset:e.offset + e.length]
                for e in message.entities if e.type == 'url']
        if (urls):
            tracks, tracks_id, group_id = pm.addTracks(urls, group.id, user.id)
            pacome.send_message(message.from_user.id, lm.get_string('add_tracks', user.user_lang).format(group.group_name), parse_mode='MarkdownV2')
            pacome.trigger('vote', {'track': tracks, 'track_id': tracks_id, 'action': 1}, user, group)
        else:
            pacome.send_message(message.from_user.id, lm.get_string('empty_msg', user.user_lang).format(group.group_name), parse_mode='MarkdownV2')
    except BadIdentifier:
        pacome.send_message(message.from_user.id, lm.get_string('bad_url', user.user_lang), parse_mode='MarkdownV2')
    except IndexError:
        pacome.send_message(message.from_user.id, lm.get_string('no_active_group', user.user_lang), parse_mode='MarkdownV2')


def removeTrack(pacome, message, user, group=None):
    # if admin removeTrackAdmin
    try:
        if not group:
            group = user.active_group
        if group == -1 or group is None:
            raise IndexError  # TODO create custom error
        urls = [message.text[e.offset:e.offset + e.length]
                for e in message.entities if e.type == 'url']
        tracks, tracks_id, group_id = pm.removeTracks(urls, group.id, user.id)
        pacome.send_message(message.from_user.id, lm.get_string('remove_tracks', user.user_lang).format(group.group_name), parse_mode='MarkdownV2')
        pacome.trigger('vote', {'track': tracks, 'track_id': tracks_id, 'action': 0}, user, group)
    except BadIdentifier:
        pacome.send_message(message.from_user.id, lm.get_string('bad_url', user.user_lang), parse_mode='MarkdownV2')
    except IndexError:
        pacome.send_message(message.from_user.id, lm.get_string('no_active_group', user.user_lang), parse_mode='MarkdownV2')


def removeTracks(pacome, message, user):
    # if admin removeTrackAdmin
    try:
        group = user.active_group
        tracks, tracks_id, group_id = pm.removeTracks(list(map(str.strip, message.content.split(','))), user.active_group, user.id)
        pacome.send_message(message.chat.id, lm.get_string('remove_tracks', user.user_lang).format(group.group_name), parse_mode='MarkdownV2')
        pacome.trigger('vote', {'track': tracks, 'track_id': tracks_id, 'action': 0}, user)
    except BadIdentifier:
        pacome.send_message(message.from_user.id, lm.get_string('bad_url', user.user_lang), parse_mode='MarkdownV2')
    except IndexError:
        pacome.send_message(message.from_user.id, lm.get_string('no_active_group', user.user_lang), parse_mode='MarkdownV2')


def createGroup(pacome, message, user):
    group_id, group_name, group_code, playlist_url = db.createGroup(message.content, user.id, "", "", 2)
    playlist_url, playlist_id, _ = pm.createPlaylist(group_id, message.content)
    db.updateGroup(group_id, group_playlist_id=playlist_id, group_playlist_url=playlist_url)
    # Make group threshold a parameter
    pacome.send_message(message.chat.id, lm.get_string('group_created', user.user_lang).format(group_name, group_code, playlist_url), parse_mode='MarkdownV2')


def deleteGroup(pacome, message, user):
    if message.content:
        group_code = message.content
        group = pacome.get_group(group_code)
    else:
        group = user.active_group
    if group:
        if pacome.check_role(user, group, ['owner']):
            try:
                pm.deletePlaylist(group.id, group.group_playlist_id)
                db.deleteGroup(group.id)
                pacome.send_message(message.chat.id, lm.get_string('group_deleted', user.user_lang).format(group.group_name), parse_mode='MarkdownV2')
            except IndexError:
                pacome.send_message(message.chat.id, lm.get_string('unknown_group', user.user_lang), parse_mode='MarkdownV2')
        else:
            pacome.send_message(message.chat.id, lm.get_string('not_authorized', user.user_lang), parse_mode='MarkdownV2')
    else:
        pacome.send_message(message.chat.id, lm.get_string('not_group', user.user_lang), parse_mode='MarkdownV2')


def myGroups(pacome, message, user):
    all_groups = pacome.get_all_roles(user)
    if user.active_group:
        active_group_id = user.active_group.id
    else:
        active_group_id = None
    groups = ['●  *{}* `{}` {}'.format(g[1], g[2], g[3]) if g[0] == active_group_id else '○  *{}* `{}` {}'.format(g[1], g[2], g[3]) for g in all_groups]
    pacome.send_message(message.from_user.id, lm.get_string('my_groups', user.user_lang).format('\n'.join(groups)), parse_mode='MarkdownV2')


def changeGroup(pacome, message, user):
    all_groups = pacome.get_all_roles(user)
    markup = telebot.types.InlineKeyboardMarkup()
    for g in all_groups:
        itembtn = telebot.types.InlineKeyboardButton(str(g[1]), callback_data=f"group{g[0]}")
        markup.add(itembtn)
    pacome.send_message(message.from_user.id, lm.get_string('change_group', user.user_lang), reply_markup=markup)


def changeLang(pacome, message, user):
    markup = telebot.types.InlineKeyboardMarkup()
    for lang_id, emoji in lm.get_emojis():
        itembtn = telebot.types.InlineKeyboardButton(str(emoji), callback_data=f"lang{lang_id}")
        markup.add(itembtn)
    pacome.send_message(message.from_user.id, "Choose your language:", reply_markup=markup)

# Callbacks


def lang_cb(pacome, call, user):
    pacome.logger.info("Received lang callback")
    user.user_lang = int(call.data.replace('lang', ''))
    pacome.send_message(call.from_user.id, lm.get_string('lang_changed', user.user_lang), parse_mode='MarkdownV2')


def group_cb(pacome, call, user):
    pacome.logger.info("Received group callback")
    user.user_active_group = int(call.data.replace('group', ''))
    pacome.send_message(call.from_user.id, lm.get_string('group_changed', user.user_lang), parse_mode='MarkdownV2')


def vote_cb(pacome, call, user):
    pacome.logger.info("Received vote callback")
    vote, track_id = call.data.replace('vote', '').split(':')
    if not db.hasVoted(user.id, track_id):
        try:
            pm.vote(user.id, track_id, int(vote))
        except BadIdentifier:
            pacome.send_message(call.from_user.id, lm.get_string('bad_url', user.user_lang), parse_mode='MarkdownV2')
        except NoTokenError:
            pacome.send_message(call.from_user.id, lm.get_string('no_login_sp', user.user_lang), parse_mode='MarkdownV2')
        else:
            pacome.send_message(call.from_user.id, lm.get_string('voted', user.user_lang), parse_mode='MarkdownV2')
    else:
        pacome.send_message(call.from_user.id, lm.get_string('already_voted', user.user_lang), parse_mode='MarkdownV2')


# def update_cb(pacome, call, user):
#     pacome.logger.info("Received update callback")
#     group_id, action = call.data.replace('update:', '').split(':')
#     if action == 0:
#         #description
#     elif action == 1:
#         #threshold
#     elif action == 2:
#         #cover


commands = {
        '/announce': {
            'exec': announce,
            'roles': ['owner', 'admin']},
        '/login': {
            'exec': login,
            'roles': None},
        '/logout': {
            'exec': logout,
            'roles': None},
        '/join': {
            'exec': join,
            'roles': None},
        '/leave': {
            'exec': leave,
            'roles': None},
        '/updatecover': {
            'exec': updateCover,
            'roles': ['owner', 'admin'],
            'content_type': 'photo'},
        '/updatedesc': {
            'exec': updateDesc,
            'roles': ['owner', 'admin']},
        '/updatethresh': {
            'exec': updateThresh,
            'roles': ['owner', 'admin']},
        '/add': {
            'exec': addTrack,
            'roles': None,
            'bound': True},
        '/remove': {
            'exec': removeTrack,
            'roles': None},
        '/create': {
            'exec': createGroup,
            'roles': None},
        '/delete': {
            'exec': deleteGroup,
            'roles': None},
        '/mygroups': {
            'exec': myGroups,
            'roles': None},
        '/info': {
            'exec': info,
            'roles': None},
        '/group': {
            'exec': changeGroup,
            'roles': None,
            'callback': group_cb},
        '/lang': {
            'exec': changeLang,
            'roles': None,
            'callback': lang_cb}
        }

broadcasts = {
        'vote': {
            'exec': vote_bc,
            'roles': None,
            'callback': vote_cb},
        'announce': {
            'exec': announce_bc,
            'roles': None}
        }


multi_commands = {}

pacome = Bot(db, lm, commands, multi_commands, broadcasts)
pacome.load()
pacome.start_polling()
