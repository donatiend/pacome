---
layout: post
title:  "I want to create a group"
date:   2020-06-09 10:15:30 +0200
categories: jekyll update
---
For Pacôme, a group is a group of peoples that collaboratively manage a playlist via its voting system. Pacôme hosts its playlists on Spotify so you need a sppotify account to create a group.
You also need to login to Spotiy to allow Pacôme to manage the playlist.
Once you have created your group you will be able to manage your playlist from Pacôme as well as from Spotify.

To create your group you have to follow this scenario :

```
/login
/create NAME
```

After creating your group, you will be given a 6 character code that you can share with whoever you want to join the group. They will then be able to join with `/join CODE`.

You can manage your group thanks to the following commands :

```
/announce TEXT          - announce TEXT to all the members of your group
/bind (in a group chat) - binds your group to the chat to allow the passive
                          sourcing of tracks
/updatedesc DESCRPTION  - updates the Spotify descrption of your playlist
/updatethresh THRESHOLD - updates the minimum difference between upvotes and
                          downvotes to allow tracks to go to the playlist
/updatecover (as a caption of an image) - Updates the cover picture of your
                                          playlist
```


Of course, everything that has been created can be deleted with the follwing commands :

```
/unbind (in a group chat) - to remove the binding of your Pacôme group to the group chat
/delete [GROUP_CODE]      - to delete group. If you do not specify a group code, Pacôme
                            will try to delete your active group
/logout                   - to remove your spotify login info from the database. If you
                            have groups, users will be notified that you need to login
                            everytime Pacôme will try to add/remove a track.
```
