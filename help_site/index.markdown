---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

layout: home
---
Pacôme is a bot made to help you share music with your friends. It has been designed to be as flexible as possible while keeping the lowest footprint of your messages.
This is achieved to a dual interface : You can interact directly with Pacôme through the commands described in the following posts as you would do with any other telegram user,
but you can also add Pacôme as an admin to a group and bind it to a Pacôme group (see _I want to create a group_) and Pacôme will read all the messages by registered users mentioning him.
The processing of the tracks will be made through private message in an effort to not spam your group. In fact, Pacôme will never send a message to your group chat nor read a message that doesn't mention him (even though it could. Be weary of what bot you add to your group chats).

If you want to contribute to this project, see [CONTRIBUTING.md][contributing]

[contributing]: https://gitlab.com/donatiend/pacome/-/blob/dev/CONTRIBUTING.md
