---
layout: page
title: About
permalink: /about/
---

This is documentation for the open source telegram bot @PacomeBot.

If you do not like it you can complain [on the issue page][issues] or check out the [contributing page][contributing]

[issues]: https://gitlab.com/donatiend/pacome/-/issues
[contributing]: https://gitlab.com/donatiend/pacome/-/blob/dev/CONTRIBUTING.md
