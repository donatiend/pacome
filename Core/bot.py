import base64
import os
import copy
import logging
import telebot
import functools
import requests
import traceback
from dotenv import load_dotenv
from Core.models import Group, User, Role, User_group_role, Chan_group
from sqlalchemy import create_engine, tuple_, and_
from sqlalchemy.sql import ClauseElement
from sqlalchemy.orm import sessionmaker

from database import PacomeDB
from sqlalchemy.exc import OperationalError

from spotify import NoTokenError


def get_or_create(session, model, defaults=None, **kwargs):
    instance = session.query(model).filter_by(**kwargs).first()
    if instance:
        return instance, False
    else:
        params = dict((k, v) for k, v in kwargs.iteritems() if not isinstance(v, ClauseElement))
        params.update(defaults or {})
        instance = model(**params)
        session.add(instance)
        return instance, True


class Bot:

    def __init__(self, db, lm, commands, multi_commands, broadcasts=None):
        load_dotenv()
        self.lm = lm
        engine = create_engine(f'postgresql://{os.getenv("DB_USERNAME")}:{os.getenv("DB_PASSWORD")}@{os.getenv("DB_HOST")}:{os.getenv("DB_PORT")}/{os.getenv("DB_NAME")}')
        self.Session = sessionmaker(bind=engine)
        self.commands = commands
        self.broadcasts = broadcasts
        self.callbacks, self.bound_command = self._extract_features()
        self.multi_commands = multi_commands
        self.states = {}

    def load(self):

        allow_bind = True
        self.photo_max_size = int(os.getenv("PHOTO_MAX_SIZE"))
        self.group_code = os.getenv("GROUP_CODE")
        self.secret_acmin_code = os.getenv("SECRET_ADMIN_CODE")
        self.telegram_token = os.getenv("TELEGRAM_TOKEN")

        telebot.logger.setLevel(logging.INFO)
        self.logger = telebot.logger

        self.bot = telebot.AsyncTeleBot(self.telegram_token)

        @self.bot.message_handler(commands=['start'])
        def start(message):
            self.logger.info("Received start command")
            session = self.Session()
            try:
                _ = get_or_create(session, User, user_tgid=str(message.from_user.id))
                self.bot.send_message(message.chat.id, self.lm.get_string('hello', 0), parse_mode='MarkdownV2')
            except OperationalError:
                self.bot.send_message(message.from_user.id, self.lm.get_string('generic_error', 0), parse_mode='MarkdownV2')
                self.bot.send_message(os.getenv("TELEGRAM_OWNER_ID"), "[HOST] There is an issue with the database. Is it running ?")
            finally:
                session.commit()
                session.close()

        @self.bot.message_handler(commands=['help'])
        def help(message):
            self.logger.info("Received help command")
            session = self.Session()
            try:
                usr = self._get_user(session, message.from_user.id)
                if usr:
                    self.bot.send_message(message.chat.id, self.lm.get_string('help', usr.user_lang), parse_mode='MarkdownV2')
            except PacomeDB.UnknownUserError:
                self.bot.send_message(message.chat.id, self.lm.get_string('help', 0), parse_mode='MarkdownV2')
            except OperationalError:
                self.bot.send_message(message.from_user.id, self.lm.get_string('generic_error', 0), parse_mode='MarkdownV2')
                self.bot.send_message(os.getenv("TELEGRAM_OWNER_ID"), "[HOST] There is an issue with the database. Is it running ?")
            finally:
                session.commit()
                session.close()

        if allow_bind:
            @self.bot.message_handler(commands=['bind'])
            def bind(message):
                self.logger.info("Received bind command")
                session = self.Session()
                try:
                    user = self._get_user(session, message.from_user.id)
                    if user:
                        group_code = message.text.replace('/bind', '').strip()
                        group = session.query(Group.id, Group.group_name)\
                            .filter_by(group_code=group_code)\
                            .first()
                        if not group:
                            self.send_message(message.chat.id, self.lm.get_string('unknown_group', user.user_lang), parse_mode='MarkdownV2')
                            return
                        if self._check_role(session, user, group, ['owner', 'admin']):
                            binding = session.query(Chan_group).filter_by(chan_tgid=str(message.chat.id)).first()
                            if binding:
                                binding.group_id = group.id
                                self.send_message(message.chat.id, self.lm.get_string('binding_updated', user.user_lang).format(group.group_name))
                            else:
                                new_binding = Chan_group(group_id=group.id,
                                                        chan_tgid=str(message.chat.id))
                                session.add(new_binding)
                                self.send_message(message.chat.id, self.lm.get_string('binding_created', user.user_lang).format(group.group_name))
                        else:
                            self.bot.send_message(message.chat.id, self.lm.get_string('not_authorized', user.user_lang), parse_mode='MarkdownV2')
                finally:
                    session.commit()
                    session.close()

            @self.bot.message_handler(commands=['unbind'])
            def unbind(message):
                self.logger.info("Received unbind command")
                session = self.Session()
                try:
                    user = self._get_user(session, message.from_user.id)
                    if user:
                        binding = session.query(Chan_group).filter(Chan_group.chan_tgid == str(message.chat.id)).first()
                        if not binding:
                            self.send_message(message.chat.id, self.lm.get_string('unknown_binding', user.user_lang))
                            return
                        group = session.query(Group).get(binding.group_id)
                        if self._check_role(session, user, group, ['owner', 'admin']):
                            session.delete(binding)
                            self.send_message(message.chat.id, self.lm.get_string('binding_deleted', user.user_lang).format(group.group_name))
                        else:
                            self.bot.send_message(message.chat.id, self.lm.get_string('not_authorized', user.user_lang), parse_mode='MarkdownV2')
                finally:
                    session.commit()
                    session.close()

        @self.bot.callback_query_handler(func=lambda call: call.data)
        def callback(call):
            self.handle_callback(call)

    def message_handler(func):
        @functools.wraps(func)
        def wrapper(self, message, *args, **kwargs):
            self.active_session = self.Session()
            try:
                user = self._get_user(self.active_session, message.from_user.id)
                if user:
                    res = func(self, message, user)
                    return res
                else:
                    return None
            except PacomeDB.UnknownUserError:
                self.bot.send_message(message.chat.id, self.lm.get_string('unknown_user', 0), parse_mode='MarkdownV2')
            except NoTokenError:
                self.bot.send_message(message.from_user.id, self.lm.get_string('no_login_sp', user.user_lang), parse_mode='MarkdownV2')
            except Exception as e:
                self.bot.send_message(message.from_user.id, self.lm.get_string('generic_error', 0), parse_mode='MarkdownV2')
                self.bot.send_message(os.getenv("TELEGRAM_OWNER_ID"), "[HOST] There has been an issue. Traceback:")
                self.bot.send_message(os.getenv("TELEGRAM_OWNER_ID"), f"{e}\n```{''.join(traceback.format_tb(e.__traceback__))}```", parse_mode='MarkdownV2')
                self.logger.error(e)
                self.logger.error(''.join(traceback.format_tb(e.__traceback__)))
            finally:
                self.active_session.commit()
                self.active_session.close()
                self.active_session = None
        return wrapper

    def start_polling(self):
        self.bot.send_message(os.getenv("TELEGRAM_OWNER_ID"), "[MAINTENANCE] Your bot has started")
        self.bot.set_update_listener(self.handle_messages)
        self.bot.polling(none_stop=True)

    def send_message(self, *args, **kwargs):
        _ = self.bot.send_message(*args, **kwargs)

    def trigger(self, broadcast, broadcast_data, owner, group=None, parse_mode='MarkdownV2'):
        session = self.Session()
        if not group:
            group = session.query(Group)\
                         .filter_by(id=owner.user_active_group)\
                         .first()

        usrs = session.query(User_group_role.user_id)\
                      .filter_by(group_id=group.id)\
                      .all()
        for usr_id in usrs:
            usr = session.query(User).get(usr_id)
            res = self.broadcasts[broadcast]['exec'](self, broadcast_data, usr, group)
            # TODO move lang here
            if res:
                self.send_message(usr.user_tgid, res[0], parse_mode=parse_mode, reply_markup=res[1])
        session.commit()
        session.close()

    def handle_callback(self, call):
        self.bot.answer_callback_query(call.id)
        self.active_session = self.Session()
        try:
            user = self._get_user(self.active_session, call.from_user.id)
            if user:
                func, roles = self._get_callback(call)
                if not roles or user.user_active_role in roles:
                    func(self, call, user)
                else:
                    self.bot.send_message(call.from_user.id, self.lm.get_string('not_authorized', user.user_lang), parse_mode='MarkdownV2')
        except PacomeDB.UnknownUserError:
            self.botsend_message(call.from_user.id, self.lm.get_string('unknown_user', 0), parse_mode='MarkdownV2')
        finally:
            self.active_session.commit()
            self.active_session.close()
            self.active_session = None

    def handle_messages(self, messages):
        for message in messages:
            if message.content_type == 'text':
                self.handle_message_text(message)
            elif message.content_type == 'photo':
                self.handle_message_photo(message)

    @message_handler
    def handle_message_photo(self, message, user):
        if message.caption_entities:
            self._photo_with_cmd(message, user)
        else:
            self._photo_no_cmd(message, user)

    @message_handler
    def handle_message_text(self, message, user):
        if message.entities:
            if message.chat.type == 'supergroup'\
               and self._check_mention(message)\
               and self._check_bound(self.active_session, message):
                if self.bound_command:
                    sqr = self.active_session.query(Chan_group.group_id)\
                                .filter_by(chan_tgid=str(message.chat.id))\
                                .subquery()
                    group = self.active_session.query(Group).filter(Group.id.in_(sqr)).first()
                    message.content = None
                    self.bound_command(self, message, user, group=group)
                    return
            self._text_with_cmd(message, user)
        else:
            self._text_no_cmd(message, user)

    def _photo_with_cmd(self, message, user):
        cmds = [e for e in message.caption_entities if e.type == 'bot_command']
        if len(cmds) == 1:

            # Parse caption
            cmd = message.caption[cmds[0].offset:cmds[0].offset + cmds[0].length]
            if cmd in ['/start', '/help', '/bind', '/unbind']:
                return
            content = message.caption.replace(cmd, '').strip()
            self.logger.info(f"Received {cmd} command")

            # Get photo
            seq = [x.file_size - self.photo_max_size for x in message.photo if x.file_size <= self.photo_max_size]
            min_idx = seq.index(max(seq))
            i_tsk = self.bot.get_file(message.photo[min_idx].file_id)
            i = i_tsk.wait()
            r = requests.get(f"https://api.telegram.org/file/bot{self.telegram_token}/{i.file_path}")
            base64_photo = base64.b64encode(r.content)

            # Pass to command
            if    (cmd in self.commands and
                   'content_type' in self.commands[cmd] and
                   self.commands[cmd]['content_type'] == 'photo'):
                if not self.commands[cmd]['roles'] or user.user_active_role in self.commands[cmd]['roles']:
                    message.content = content
                    self.commands[cmd]['exec'](self, message, user, base64_photo)
                else:
                    self.bot.send_message(message.chat.id, self.lm.get_string('not_authorized', user.user_lang), parse_mode='MarkdownV2')
            elif cmd in self.multi_commands:
                # TODO
                print("Photo multi-command or scenario")
        elif len(cmds) > 1:
            # TODO
            print("Chained commands")

    # pylama:ignore=C901
    def _text_with_cmd(self, message, user):
        cmds = [e for e in message.entities if e.type == 'bot_command']
        if len(cmds) == 1:
            cmd = message.text[cmds[0].offset:cmds[0].offset + cmds[0].length]
            if cmd in ['/start', '/help', '/bind', '/unbind']:
                return
            content = message.text.replace(cmd, '').strip()
            self.logger.info(f"Received {cmd} command")
            # TODO only go through text commands
            if cmd in self.commands:
                if not self.commands[cmd]['roles'] or user.user_active_role in self.commands[cmd]['roles']:
                    message.content = content
                    self.commands[cmd]['exec'](self, message, user)
                else:
                    self.bot.send_message(message.chat.id, self.lm.get_string('not_authorized', user.user_lang), parse_mode='MarkdownV2')
            elif cmd in self.multi_commands:
                if not self.multi_commands[cmd]['roles'] or user.user_active_role in self.multi_commands[cmd]['roles']:
                    if content:
                        cts = content.split(';')
                        for i, c in enumerate(cts):
                            msg = copy.deepcopy(message)
                            msg.content = c
                            self.multi_commands[cmd]['commands'][i](self, msg, user)
                    else:
                        if self.multi_commands[cmd]['scenario']:
                            message.content = content
                            self.states[message.from_user.id] = cmd + '|0'
                            self.multi_commands[cmd]['init'](self, message, user)
                        else:
                            self.bot.send_message(message.chat.id, self.lm.get_string('content_needed', user.user_lang), parse_mode='MarkdownV2')
                else:
                    self.bot.send_message(message.chat.id, self.lm.get_string('not_authorized', user.user_lang), parse_mode='MarkdownV2')
            else:
                self.bot.send_message(message.chat.id, self.lm.get_string('unknown_command', user.user_lang), parse_mode='MarkdownV2')
        elif len(cmds) > 1:
            print("Chained commands")

    def _text_no_cmd(self, message, user):
        self.logger.info("Received commandless command")
        if message.from_user.id in self.states:
            scenario, sstep = self.states[message.from_user.id].split('|')
            step = int(sstep)
            if step < len(self.multi_commands[scenario]['commands']) - 1:
                self.states[message.from_user.id] = scenario + '|' + str(step + 1)
                self.multi_commands[scenario]['commands'][step](self, message, user)
            else:
                self.states.pop(message.from_user.id, None)
                self.multi_commands[scenario]['commands'][step](self, message, user)

    def _get_user(self, session, tg_id):
        try:
            usr = session.query(User).filter_by(user_tgid=str(tg_id)).first()
            if usr and usr.user_active_group:
                role_id = session.query(User_group_role.role_id)\
                                .filter_by(group_id=usr.user_active_group,
                                           user_id=usr.id)\
                                .first()
                usr.user_active_role = session.query(Role.name)\
                                              .filter_by(id=role_id)\
                                              .first()[0]
            return usr
        except OperationalError:
            self.bot.send_message(tg_id, self.lm.get_string('generic_error', 0), parse_mode='MarkdownV2')
            self.bot.send_message(os.getenv("TELEGRAM_OWNER_ID"), "[HOST] There is an issue with the database. Is it running ?")

    def _get_callback(self, call):
        for cb in self.callbacks:
            if call.data.startswith(cb['command'].replace('/', '')):
                return cb['exec'], cb['roles']

    def _extract_features(self):
        cbks = []
        bound = None
        for cmd in self.commands:
            if 'callback' in self.commands[cmd]:
                cbks.append({"command": cmd,
                             "exec": self.commands[cmd]['callback'],
                             "roles": self.commands[cmd]['roles']})
            elif 'bound' in self.commands[cmd] and self.commands[cmd]['bound']:
                bound = self.commands[cmd]['exec']
        for bcst in self.broadcasts:
            if 'callback' in self.broadcasts[bcst]:
                cbks.append({"command": bcst,
                             "exec": self.broadcasts[bcst]['callback'],
                             "roles": self.broadcasts[bcst]['roles']})
        return cbks, bound

    def _check_role(self, session, user, group, roles):
        sqr = session.query(Role.id).filter(Role.name.in_(roles)).subquery()
        rest = session.query(User_group_role)\
                      .filter(and_(User_group_role.user_id == user.id,
                                   User_group_role.group_id == group.id,
                                   User_group_role.role_id.in_(sqr)))\
                      .count()
        return bool(rest)

    def _check_mention(self, message):
        return bool([e for e in message.entities
                    if e.type == 'mention'
                    and message.text[e.offset:e.offset + e.length] == f'@{self.bot.get_me().wait().username}'])

    def _check_bound(self, session, message):
        return bool(session.query(Chan_group)
                           .filter(Chan_group.chan_tgid == str(message.chat.id))
                           .count())

    def check_role(self, user, group, roles):
        return self._check_role(self.active_session, user, group, roles)

    def get_group(self, group_code):
        session = self.active_session
        group = session.query(Group).filter_by(group_code=group_code).first()
        return group

    def get_all_roles(self, user):
        session = self.active_session
        sqr = session.query(User_group_role.group_id, User_group_role.role_id)\
                     .filter_by(user_id=user.id)\
                     .subquery()
        roles = session.query(Group.id,
                              Group.group_name,
                              Group.group_code,
                              Role.name)\
                       .filter(tuple_(Group.id, Role.id).in_(sqr))\
                       .all()
        return roles

    def get_members(self, group):
        session = self.active_session
        r = session.query(User_group_role.id).filter_by(group_id=group.id).all()
        return r
