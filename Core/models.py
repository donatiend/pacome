from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()


class Group(Base):
    __tablename__ = 'groups'

    id = Column(Integer, primary_key=True)
    group_code = Column(String)
    group_name = Column(String)
    group_playlist_url = Column(String)
    group_playlist_id = Column(String)
    group_threshold = Column(Integer, default=0)

    def __repr__(self):
        return f"<Group(id={self.id}, group_name='{self.group_name}', group_code={self.group_code}, group_threshold={self.group_threshold})>"


class User(Base):
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True)
    user_tgid = Column(String)
    user_active_group = Column(Integer, ForeignKey(Group.id))
    user_lang = Column(Integer, default=0)

    user_active_role = None
    active_group = relationship(Group, load_on_pending=True)

    def __repr__(self):
        return f"<User(id={self.id}, user_tgid='{self.user_tgid}', user_active_group={self.user_active_group}, user_lang={self.user_lang}, user_active_role={self.user_active_role})>"


class Role(Base):
    __tablename__ = 'roles'

    id = Column(Integer, primary_key=True)
    name = Column(String)
    description = Column(String)


class User_group_role(Base):
    __tablename__ = 'user_group_role'

    id = Column(Integer, primary_key=True)
    user_id = Column(Integer, ForeignKey(User.id))
    group_id = Column(Integer, ForeignKey(Group.id))
    role_id = Column(Integer, ForeignKey(Role.id))


class Chan_group(Base):
    __tablename__ = 'chan_group'

    id = Column(Integer, primary_key=True)
    group_id = Column(Integer, ForeignKey(Group.id))
    chan_tgid = Column(String)
